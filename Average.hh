#pragma once

namespace kt84 {

template <typename EigenType>
struct Average {
    EigenType sum;
    double weightSum;
    Average() : sum(EigenType::Zero()), weightSum(0) {}
    Average(const EigenType& value, double weight = 1) : sum(value), weightSum(weight) {}
    void add(const EigenType& x, double weight = 1) {
        sum += weight*x;
        weightSum += weight;
    }
    EigenType value() const {
        if (weightSum==0)
            return EigenType::Zero();
        return sum / weightSum;
    }
    void setZero() { *this = {}; }
    void setValue(const EigenType& value, double weight = 1) { *this = {value, weight}; }
};

struct Average1d {
    double sum;
    double weightSum;
    Average1d() : sum(0), weightSum(0) {}
    Average1d(double value, double weight = 1) : sum(value), weightSum(weight) {}
    void add(double x, double weight = 1) {
        sum += weight*x;
        weightSum += weight;
    }
    double value() const {
        if (weightSum==0)
            return 0;
        return sum / weightSum;
    }
    void setZero() { *this = {}; }
    void setValue(const double value, double weight = 1) { *this = {value, weight}; }
};

}
