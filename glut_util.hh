#pragma once
#include <GL/freeglut.h>

namespace kt84 {
    namespace glut_util {
        namespace defaultcb {
            inline void display() {
                glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                glBegin(GL_QUADS);
                glColor3d(0.8, 0.8, 0.8);    glVertex2d(-1, -1);    glVertex2d( 1, -1);    // bottom
                glColor3d(0.2, 0.2, 0.2);    glVertex2d( 1,  1);    glVertex2d(-1,  1);    // top
                glEnd();
#ifdef TW_INCLUDED
                TwDraw();
#endif
                glutSwapBuffers();
                glutReportErrors();
            }
            inline void reshape(int width, int height) {
                glViewport(0, 0, width, height);
#ifdef TW_INCLUDED
                TwWindowSize(width, height);
#endif
            }
            inline void keyboard(unsigned char key, int x, int y) {
#ifdef TW_INCLUDED
                if (TwEventKeyboardGLUT(key, x, y)) return glutPostRedisplay();
#endif
            }
            inline void special(int key, int x, int y) {
#ifdef TW_INCLUDED
                if (TwEventSpecialGLUT(key, x, y)) return glutPostRedisplay();
#endif
            }
            inline void mouse(int glut_button, int state, int x, int y) {
#ifdef TW_INCLUDED
                if (TwEventMouseButtonGLUT(glut_button, state, x, y)) return glutPostRedisplay();
#endif
            }
            inline void motion(int x, int y) {
#ifdef TW_INCLUDED
                if (TwEventMouseMotionGLUT(x, y)) return glutPostRedisplay();
#endif
            }
        }
        inline void init(
            int argc, char* argv[],
            unsigned int display_mode = GLUT_DOUBLE | GLUT_RGBA,
            int window_posx = -1,
            int window_posy = -1,
            int window_width = 0,
            int window_height = 0,
            const char* window_title = "glut_window",
            void (*display)()                                       = defaultcb::display,
            void (*reshape)(int width, int height)                  = defaultcb::reshape,
            void (*keyboard)(unsigned char key, int x, int y)       = defaultcb::keyboard,
            void (*keyboardup)(unsigned char key, int x, int y)     = nullptr,
            void (*special)(int key, int x, int y)                  = defaultcb::special,
            void (*mouse)(int glut_button, int state, int x, int y) = defaultcb::mouse,
            void (*motion)(int x, int y)                            = defaultcb::motion, 
            void (*passive_motion)(int x, int y)                    = defaultcb::motion,
            void (*idle)()                                          = nullptr)
        {
            glutInit(&argc, argv);
            
            // Create window
            glutInitDisplayMode (display_mode);
            if (window_posx >= 0 && window_posy >= 0)
                glutInitWindowPosition(window_posx, window_posy);
            if (window_width > 0 && window_height > 0)
                glutInitWindowSize(window_width, window_height);
            glutCreateWindow(window_title);
            
            glutDisplayFunc(display);
            glutReshapeFunc(reshape);
            glutKeyboardFunc(keyboard);
            glutKeyboardUpFunc(keyboardup);
            glutSpecialFunc(special);
            glutMouseFunc(mouse);
            glutMotionFunc(motion);
            glutPassiveMotionFunc(passive_motion);
            glutIdleFunc(idle);
        }
        namespace internal {
            struct ModifiersFlag {
                bool shift, ctrl, alt;
            };
        }
        inline internal::ModifiersFlag get_modifiers_flag() {
            return {
                (glutGetModifiers() & GLUT_ACTIVE_SHIFT) != 0,
                (glutGetModifiers() & GLUT_ACTIVE_CTRL ) != 0,
                (glutGetModifiers() & GLUT_ACTIVE_ALT  ) != 0
            };
        }
    }
}
