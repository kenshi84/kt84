#pragma once
#include <limits>
#include <ostream>
#include <utility>
#undef min
#undef max

namespace kt84 {

template <typename Value>
struct MaxMinSelector {
    double max_score = -std::numeric_limits<double>::max(),
           min_score =  std::numeric_limits<double>::max();
    double total_score = 0;
    size_t count = 0;
    Value max_value,
          min_value;
    MaxMinSelector(const Value& init_value = Value())
        : max_value(init_value)
        , min_value(init_value)
    {}
    std::pair<bool,bool> update(double scoreNew, const Value& valueNew) {
        total_score += (double)scoreNew;
        ++count;
        auto updated = std::make_pair(false,false);
        if (scoreNew > max_score) {
            max_score = scoreNew;
            max_value = valueNew;
            updated.first = true;
        }
        if (scoreNew < min_score) {
            min_score = scoreNew;
            min_value = valueNew;
            updated.second = true;
        }
        return updated;
    }
    double average_score() const { return total_score / count; }
};

template <typename Value>
struct MaxSelector {
    double score = -std::numeric_limits<double>::max();
    double total_score = 0;
    size_t count = 0;
    Value value;
    MaxSelector(const Value& init_value = Value())
        : value(init_value)
    {}
    bool update(double scoreNew, const Value& valueNew) {
        total_score += (double)scoreNew;
        ++count;
        if (scoreNew > score) {
            score = scoreNew;
            value = valueNew;
            return true;
        }
        return false;
    }
    double average_score() const { return total_score / count; }
};

template <typename Value>
struct MinSelector {
    double score = std::numeric_limits<double>::max();
    double total_score = 0;
    size_t count = 0;
    Value value;
    MinSelector(const Value& init_value = Value())
        : value(init_value)
    {}
    bool update(double scoreNew, const Value& valueNew) {
        total_score += (double)scoreNew;
        ++count;
        if (scoreNew < score) {
            score = scoreNew;
            value = valueNew;
            return true;
        }
        return false;
    }
    double average_score() const { return total_score / count; }
};

struct MaxMinAverage {
    MaxMinSelector<int> selector;
    std::pair<bool,bool> update(double score) { return selector.update(score, 0); }
    double max() const { return selector.max_score; }
    double min() const { return selector.min_score; }
    double avg() const { return selector.average_score(); }
    size_t count() const { return selector.count; }
    void reset() { selector = {}; }
};

inline std::ostream& operator<<(std::ostream& out, const kt84::MaxMinAverage& mma) {
    out << "max=" << mma.max()
        << ", min=" << mma.min()
        << ", avg=" << mma.avg()
        << ", count=" << mma.count();
    return out;
}

}
