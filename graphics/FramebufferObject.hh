#pragma once
#include <vector>
#include <GL/glew.h>
#include <kt84/graphics/TextureObjectT.hh>
#include <kt84/graphics/RenderbufferObject.hh>

namespace kt84 {

struct FramebufferObject {
    GLuint handle;
    
    FramebufferObject()
        : handle(0)
    {}
    
    void init() { glGenFramebuffersEXT(1, &handle); }
    
    void bind(GLenum target = GL_FRAMEBUFFER_EXT) const { glBindFramebufferEXT(target, handle); }

    void blit(const FramebufferObject& draw_fbo, int width, int height, GLbitfield mask = GL_DEPTH_BUFFER_BIT /* or GL_COLOR_BUFFER_BIT or GL_STENCIL_BUFFER_BIT */, GLenum filter = GL_NEAREST /* or GL_LINEAR */) const {
        bind(GL_READ_FRAMEBUFFER_EXT);
        draw_fbo.bind(GL_DRAW_FRAMEBUFFER_EXT);
        glBlitFramebufferEXT(0, 0, width, height, 0, 0, width, height, mask, filter);
        unbind();
    }
    
    static void unbind() { glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); }
    
    bool is_bound() const {
        GLint current_handle;
        glGetIntegerv(GL_FRAMEBUFFER_BINDING_EXT, &current_handle);
        return handle == current_handle;
    }

    static bool is_complete() {
        return glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;
    }
    
    template <GLenum Target>
    static void attach_texture(GLenum attachment, const TextureObjectT<Target>& texture) {
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, attachment, Target, texture.handle, 0);
    }
    template <GLenum Target>
    static void attach_texture_color(GLuint attachment_index, const TextureObjectT<Target>& texture) {
        attach_texture(GL_COLOR_ATTACHMENT0 + attachment_index, texture);
    }
    template <GLenum Target>
    static void detach_texture(GLenum attachment, const TextureObjectT<Target>& texture) {
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, attachment, Target, 0, 0);
    }
    static void set_draw_buffers_color(size_t n) {
        std::vector<GLuint> buf(n);
        for (size_t i = 0; i < buf.size(); ++i)
            buf[i] = GL_COLOR_ATTACHMENT0 + i;
        glDrawBuffers(n, buf.data());
    }
    
    static void attach_renderbuffer(GLenum attachment, const RenderbufferObject& renderbuffer) {
        glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, attachment, GL_RENDERBUFFER_EXT, renderbuffer.handle);
    }
    static void attach_renderbuffer_depth(const RenderbufferObject& renderbuffer) {
        attach_renderbuffer(GL_DEPTH_ATTACHMENT, renderbuffer);
    }
    static void detach_renderbuffer(GLenum attachment) {
        glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, attachment, GL_RENDERBUFFER_EXT, 0);
    }
};

}
