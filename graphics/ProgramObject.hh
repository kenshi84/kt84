#pragma once

#include <kt84/graphics/ShaderObject.hh>
#include <unordered_map>

namespace kt84 {

struct ProgramObject {
    GLuint handle;
    std::unordered_map<std::string, GLint> attribloc;
    
    ProgramObject()
        : handle(0)
    {}
    void init() {
        handle = glCreateProgramObjectARB();
    }
    void attach(const ShaderObject& s) {
        glAttachObjectARB(handle, s.handle);
    }
    void link(void) const {
        glLinkProgramARB(handle);
        GLint result;
        glGetObjectParameterivARB(handle, GL_OBJECT_LINK_STATUS_ARB, &result);
        if (result == GL_FALSE) {
            int length;
            glGetObjectParameterivARB(handle, GL_OBJECT_INFO_LOG_LENGTH_ARB, &length);
            if (length > 0) {
                int l;
                std::string info_log;
                info_log.resize(length);
                glGetInfoLogARB(handle, length, &l, &info_log[0]);
                std::cerr << info_log << std::endl;
            }
        }
    }
    void init_compile_attach_link(const std::string& frag_source, const std::string& vert_source, const std::string& geom_source = "") {
        init();

        ShaderObject frag;
        frag.init(ShaderObject::Type::FRAGMENT_SHADER);
        frag.set_source(frag_source);
        frag.compile();
        attach(frag);

        ShaderObject vert;
        vert.init(ShaderObject::Type::VERTEX_SHADER);
        vert.set_source(vert_source);
        vert.compile();
        attach(vert);

        if (!geom_source.empty()) {
            ShaderObject geom;
            geom.init(ShaderObject::Type::GEOMETRY_SHADER);
            geom.set_source(geom_source);
            geom.compile();
            attach(geom);
        }

        link();
    }
    void enable() const { glUseProgramObjectARB(handle); }
    static void disable() { glUseProgramObjectARB(0); }

    GLint get_uniform_location(const std::string& name) const {
        GLint ul = glGetUniformLocationARB(handle, name.c_str());
        if (ul == -1) {
            std::cerr << "no such uniform named " << name << std::endl;
        }
        return ul;
    }

    void register_attrib_location(const std::string& name) {
        if (attribloc.count(name) > 0) {
            std::cerr << "attrib name " << name << " was already registered" << std::endl;
            return;
        }
        GLint al = glGetAttribLocationARB(handle, name.c_str());
        if (al == -1) {
            std::cerr << "no such attrib named " << name << std::endl;
            return;
        }
        attribloc[name] = al;
    }

#pragma warning(disable: 4244)
    
    // glUniform<N><T>
    template <typename T>
    void set_uniform_1(const std::string& name, T v0) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform1i (location, v0);
        else if (t == typeid(GLuint  )) glUniform1ui(location, v0);
        else if (t == typeid(GLfloat )) glUniform1f (location, v0);
        else if (t == typeid(GLdouble)) glUniform1d (location, v0);
    }
    template <typename T>
    void set_uniform_2(const std::string& name, T v0, T v1) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform2i (location, v0, v1);
        else if (t == typeid(GLuint  )) glUniform2ui(location, v0, v1);
        else if (t == typeid(GLfloat )) glUniform2f (location, v0, v1);
        else if (t == typeid(GLdouble)) glUniform2d (location, v0, v1);
    }
    template <typename T>
    void set_uniform_3(const std::string& name, T v0, T v1, T v2) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform3i (location, v0, v1, v2);
        else if (t == typeid(GLuint  )) glUniform3ui(location, v0, v1, v2);
        else if (t == typeid(GLfloat )) glUniform3f (location, v0, v1, v2);
        else if (t == typeid(GLdouble)) glUniform3d (location, v0, v1, v2);
    }
    template <typename T>
    void set_uniform_4(const std::string& name, T v0, T v1, T v2, T v3) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform4i (location, v0, v1, v2, v3);
        else if (t == typeid(GLuint  )) glUniform4ui(location, v0, v1, v2, v3);
        else if (t == typeid(GLfloat )) glUniform4f (location, v0, v1, v2, v3);
        else if (t == typeid(GLdouble)) glUniform4d (location, v0, v1, v2, v3);
    }
    template <class TVector> void set_uniform_2( const std::string& name, const TVector& v ) const { set_uniform_2(name, v[0], v[1]); }
    template <class TVector> void set_uniform_3( const std::string& name, const TVector& v ) const { set_uniform_3(name, v[0], v[1], v[2]); }
    template <class TVector> void set_uniform_4( const std::string& name, const TVector& v ) const { set_uniform_4(name, v[0], v[1], v[2], v[3]); }
    
    // glUniform<N><T>v
    template <typename T>
    void set_uniform_1v( const std::string& name, GLuint count, const T *v ) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform1iv (location, count, (GLint    *)v);
        else if (t == typeid(GLuint  )) glUniform1uiv(location, count, (GLuint   *)v);
        else if (t == typeid(GLfloat )) glUniform1fv (location, count, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glUniform1dv (location, count, (GLdouble *)v);
    }
    template <typename T>
    void set_uniform_2v( const std::string& name, GLuint count, const T *v ) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform2iv (location, count, (GLint    *)v);
        else if (t == typeid(GLuint  )) glUniform2uiv(location, count, (GLuint   *)v);
        else if (t == typeid(GLfloat )) glUniform2fv (location, count, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glUniform2dv (location, count, (GLdouble *)v);
    }
    template <typename T>
    void set_uniform_3v( const std::string& name, GLuint count, const T *v ) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform3iv (location, count, (GLint    *)v);
        else if (t == typeid(GLuint  )) glUniform3uiv(location, count, (GLuint   *)v);
        else if (t == typeid(GLfloat )) glUniform3fv (location, count, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glUniform3dv (location, count, (GLdouble *)v);
    }
    template <typename T>
    void set_uniform_4v( const std::string& name, GLuint count, const T *v ) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLint   )) glUniform4iv (location, count, (GLint    *)v);
        else if (t == typeid(GLuint  )) glUniform4uiv(location, count, (GLuint   *)v);
        else if (t == typeid(GLfloat )) glUniform4fv (location, count, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glUniform4dv (location, count, (GLdouble *)v);
    }
    
    // glUniformMatrix<N><T>v
    template <typename T>
    void set_uniform_matrix_2v( const std::string& name, GLuint count, bool col_major, const T *v ) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLfloat )) glUniformMatrix2fv(location, count, col_major ? GL_FALSE : GL_TRUE, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glUniformMatrix2dv(location, count, col_major ? GL_FALSE : GL_TRUE, (GLdouble *)v);
    }
    template <typename T>
    void set_uniform_matrix_3v( const std::string& name, GLuint count, bool col_major, const T *v ) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLfloat )) glUniformMatrix3fv(location, count, col_major ? GL_FALSE : GL_TRUE, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glUniformMatrix3dv(location, count, col_major ? GL_FALSE : GL_TRUE, (GLdouble *)v);
    }
    template <typename T>
    void set_uniform_matrix_4v( const std::string& name, GLuint count, bool col_major, const T *v ) const {
        auto& t = typeid(T);
        GLint location = get_uniform_location(name);
        if      (t == typeid(GLfloat )) glUniformMatrix4fv(location, count, col_major ? GL_FALSE : GL_TRUE, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glUniformMatrix4dv(location, count, col_major ? GL_FALSE : GL_TRUE, (GLdouble *)v);
    }
    template <class TMatrix> void set_uniform_matrix_2( const std::string& name, const TMatrix& m, bool col_major = true) const { set_uniform_matrix_2v( name, 1, col_major, &m(0, 0)); }
    template <class TMatrix> void set_uniform_matrix_3( const std::string& name, const TMatrix& m, bool col_major = true) const { set_uniform_matrix_3v( name, 1, col_major, &m(0, 0)); }
    template <class TMatrix> void set_uniform_matrix_4( const std::string& name, const TMatrix& m, bool col_major = true) const { set_uniform_matrix_4v( name, 1, col_major, &m(0, 0)); }
    
    // glVertexattrib<N><T>
    template <typename T>
    void set_attrib_1(const std::string& name, T v0) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib1f(location, v0);
        else if (t == typeid(GLdouble)) glVertexAttrib1d(location, v0);
        else if (t == typeid(GLshort )) glVertexAttrib1s(location, v0);
    }
    template <typename T>
    void set_attrib_2(const std::string& name, T v0, T v1) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib2f(location, v0, v1);
        else if (t == typeid(GLdouble)) glVertexAttrib2d(location, v0, v1);
        else if (t == typeid(GLshort )) glVertexAttrib2s(location, v0, v1);
    }
    template <typename T>
    void set_attrib_3(const std::string& name, T v0, T v1, T v2) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib3f(location, v0, v1, v2);
        else if (t == typeid(GLdouble)) glVertexAttrib3d(location, v0, v1, v2);
        else if (t == typeid(GLshort )) glVertexAttrib3s(location, v0, v1, v2);
    }
    template <typename T>
    void set_attrib_4(const std::string& name, T v0, T v1, T v2, T v3) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib4f(location, v0, v1, v2, v3);
        else if (t == typeid(GLdouble)) glVertexAttrib4d(location, v0, v1, v2, v3);
        else if (t == typeid(GLshort )) glVertexAttrib4s(location, v0, v1, v2, v3);
    }
    template <class TVector> void set_attrib_2( const std::string& name, const TVector& v ) const { set_attrib_2( name, v[0], v[1] ); }
    template <class TVector> void set_attrib_3( const std::string& name, const TVector& v ) const { set_attrib_3( name, v[0], v[1], v[2] ); }
    template <class TVector> void set_attrib_4( const std::string& name, const TVector& v ) const { set_attrib_4( name, v[0], v[1], v[2], v[3] ); }
    
    // glVertexattrib<N><T>v
    template <typename T>
    void set_attrib_1v( const std::string& name, const T *v ) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib1fv(location, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glVertexAttrib1dv(location, (GLdouble *)v);
        else if (t == typeid(GLshort )) glVertexAttrib1sv(location, (GLshort  *)v);
    }
    template <typename T>
    void set_attrib_2v( const std::string& name, const T *v ) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib2fv(location, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glVertexAttrib2dv(location, (GLdouble *)v);
        else if (t == typeid(GLshort )) glVertexAttrib2sv(location, (GLshort  *)v);
    }
    template <typename T>
    void set_attrib_3v( const std::string& name, const T *v ) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib3fv(location, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glVertexAttrib3dv(location, (GLdouble *)v);
        else if (t == typeid(GLshort )) glVertexAttrib3sv(location, (GLshort  *)v);
    }
    template <typename T>
    void set_attrib_4v( const std::string& name, const T *v ) const {
        auto& t = typeid(T);
        GLint location = attribloc.at(name);
        if      (t == typeid(GLfloat )) glVertexAttrib4fv(location, (GLfloat  *)v);
        else if (t == typeid(GLdouble)) glVertexAttrib4dv(location, (GLdouble *)v);
        else if (t == typeid(GLshort )) glVertexAttrib4sv(location, (GLshort  *)v);
    }

    // explicit type specification to avoid unintended behavior
    void set_uniform_1i (const std::string& name, GLint    v0) const { set_uniform_1(name, v0); }
    void set_uniform_1ui(const std::string& name, GLuint   v0) const { set_uniform_1(name, v0); }
    void set_uniform_1f (const std::string& name, GLfloat  v0) const { set_uniform_1(name, v0); }
    void set_uniform_1d (const std::string& name, GLdouble v0) const { set_uniform_1(name, v0); }
    void set_uniform_2i (const std::string& name, GLint    v0, GLint    v1) const { set_uniform_2(name, v0, v1); }
    void set_uniform_2ui(const std::string& name, GLuint   v0, GLuint   v1) const { set_uniform_2(name, v0, v1); }
    void set_uniform_2f (const std::string& name, GLfloat  v0, GLfloat  v1) const { set_uniform_2(name, v0, v1); }
    void set_uniform_2d (const std::string& name, GLdouble v0, GLdouble v1) const { set_uniform_2(name, v0, v1); }
    void set_uniform_3i (const std::string& name, GLint    v0, GLint    v1, GLint    v2) const { set_uniform_3(name, v0, v1, v2); }
    void set_uniform_3ui(const std::string& name, GLuint   v0, GLuint   v1, GLuint   v2) const { set_uniform_3(name, v0, v1, v2); }
    void set_uniform_3f (const std::string& name, GLfloat  v0, GLfloat  v1, GLfloat  v2) const { set_uniform_3(name, v0, v1, v2); }
    void set_uniform_3d (const std::string& name, GLdouble v0, GLdouble v1, GLdouble v2) const { set_uniform_3(name, v0, v1, v2); }
    void set_uniform_4i (const std::string& name, GLint    v0, GLint    v1, GLint    v2, GLint    v3) const { set_uniform_4(name, v0, v1, v2, v3); }
    void set_uniform_4ui(const std::string& name, GLuint   v0, GLuint   v1, GLuint   v2, GLuint   v3) const { set_uniform_4(name, v0, v1, v2, v3); }
    void set_uniform_4f (const std::string& name, GLfloat  v0, GLfloat  v1, GLfloat  v2, GLfloat  v3) const { set_uniform_4(name, v0, v1, v2, v3); }
    void set_uniform_4d (const std::string& name, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3) const { set_uniform_4(name, v0, v1, v2, v3); }
    template <class TVector> void set_uniform_2i ( const std::string& name, const TVector& v ) const { set_uniform_2i (name, v[0], v[1]); }
    template <class TVector> void set_uniform_2ui( const std::string& name, const TVector& v ) const { set_uniform_2ui(name, v[0], v[1]); }
    template <class TVector> void set_uniform_2f ( const std::string& name, const TVector& v ) const { set_uniform_2f (name, v[0], v[1]); }
    template <class TVector> void set_uniform_2d ( const std::string& name, const TVector& v ) const { set_uniform_2d (name, v[0], v[1]); }
    template <class TVector> void set_uniform_3i ( const std::string& name, const TVector& v ) const { set_uniform_3i (name, v[0], v[1], v[2]); }
    template <class TVector> void set_uniform_3ui( const std::string& name, const TVector& v ) const { set_uniform_3ui(name, v[0], v[1], v[2]); }
    template <class TVector> void set_uniform_3f ( const std::string& name, const TVector& v ) const { set_uniform_3f (name, v[0], v[1], v[2]); }
    template <class TVector> void set_uniform_3d ( const std::string& name, const TVector& v ) const { set_uniform_3d (name, v[0], v[1], v[2]); }
    template <class TVector> void set_uniform_4i ( const std::string& name, const TVector& v ) const { set_uniform_4i (name, v[0], v[1], v[2], v[3]); }
    template <class TVector> void set_uniform_4ui( const std::string& name, const TVector& v ) const { set_uniform_4ui(name, v[0], v[1], v[2], v[3]); }
    template <class TVector> void set_uniform_4f ( const std::string& name, const TVector& v ) const { set_uniform_4f (name, v[0], v[1], v[2], v[3]); }
    template <class TVector> void set_uniform_4d ( const std::string& name, const TVector& v ) const { set_uniform_4d (name, v[0], v[1], v[2], v[3]); }
    void set_attrib_1i (const std::string& name, GLint    v0) const { set_attrib_1(name, v0); }
    void set_attrib_1ui(const std::string& name, GLuint   v0) const { set_attrib_1(name, v0); }
    void set_attrib_1f (const std::string& name, GLfloat  v0) const { set_attrib_1(name, v0); }
    void set_attrib_1d (const std::string& name, GLdouble v0) const { set_attrib_1(name, v0); }
    void set_attrib_2i (const std::string& name, GLint    v0, GLint    v1) const { set_attrib_2(name, v0, v1); }
    void set_attrib_2ui(const std::string& name, GLuint   v0, GLuint   v1) const { set_attrib_2(name, v0, v1); }
    void set_attrib_2f (const std::string& name, GLfloat  v0, GLfloat  v1) const { set_attrib_2(name, v0, v1); }
    void set_attrib_2d (const std::string& name, GLdouble v0, GLdouble v1) const { set_attrib_2(name, v0, v1); }
    void set_attrib_3i (const std::string& name, GLint    v0, GLint    v1, GLint    v2) const { set_attrib_3(name, v0, v1, v2); }
    void set_attrib_3ui(const std::string& name, GLuint   v0, GLuint   v1, GLuint   v2) const { set_attrib_3(name, v0, v1, v2); }
    void set_attrib_3f (const std::string& name, GLfloat  v0, GLfloat  v1, GLfloat  v2) const { set_attrib_3(name, v0, v1, v2); }
    void set_attrib_3d (const std::string& name, GLdouble v0, GLdouble v1, GLdouble v2) const { set_attrib_3(name, v0, v1, v2); }
    void set_attrib_4i (const std::string& name, GLint    v0, GLint    v1, GLint    v2, GLint    v3) const { set_attrib_4(name, v0, v1, v2, v3); }
    void set_attrib_4ui(const std::string& name, GLuint   v0, GLuint   v1, GLuint   v2, GLuint   v3) const { set_attrib_4(name, v0, v1, v2, v3); }
    void set_attrib_4f (const std::string& name, GLfloat  v0, GLfloat  v1, GLfloat  v2, GLfloat  v3) const { set_attrib_4(name, v0, v1, v2, v3); }
    void set_attrib_4d (const std::string& name, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3) const { set_attrib_4(name, v0, v1, v2, v3); }
    template <class TVector> void set_attrib_2i ( const std::string& name, const TVector& v ) const { set_attrib_2i ( name, v[0], v[1] ); }
    template <class TVector> void set_attrib_2ui( const std::string& name, const TVector& v ) const { set_attrib_2ui( name, v[0], v[1] ); }
    template <class TVector> void set_attrib_2f ( const std::string& name, const TVector& v ) const { set_attrib_2f ( name, v[0], v[1] ); }
    template <class TVector> void set_attrib_2d ( const std::string& name, const TVector& v ) const { set_attrib_2d ( name, v[0], v[1] ); }
    template <class TVector> void set_attrib_3i ( const std::string& name, const TVector& v ) const { set_attrib_3i ( name, v[0], v[1], v[2] ); }
    template <class TVector> void set_attrib_3ui( const std::string& name, const TVector& v ) const { set_attrib_3ui( name, v[0], v[1], v[2] ); }
    template <class TVector> void set_attrib_3f ( const std::string& name, const TVector& v ) const { set_attrib_3f ( name, v[0], v[1], v[2] ); }
    template <class TVector> void set_attrib_3d ( const std::string& name, const TVector& v ) const { set_attrib_3d ( name, v[0], v[1], v[2] ); }
    template <class TVector> void set_attrib_4i ( const std::string& name, const TVector& v ) const { set_attrib_4i ( name, v[0], v[1], v[2], v[3] ); }
    template <class TVector> void set_attrib_4ui( const std::string& name, const TVector& v ) const { set_attrib_4ui( name, v[0], v[1], v[2], v[3] ); }
    template <class TVector> void set_attrib_4f ( const std::string& name, const TVector& v ) const { set_attrib_4f ( name, v[0], v[1], v[2], v[3] ); }
    template <class TVector> void set_attrib_4d ( const std::string& name, const TVector& v ) const { set_attrib_4d ( name, v[0], v[1], v[2], v[3] ); }

#pragma warning(default: 4244)
};

}

