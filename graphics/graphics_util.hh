#pragma once
#include <GL/glew.h>
#include <Eigen/Core>

namespace kt84 {

namespace graphics_util {
    enum struct ParamName {
#if defined(KT84_USE_OPENGL_4)
#   include <kt84/graphics/ParamName4.inl>
#elif defined(KT84_USE_OPENGL_3_3)
#   include <kt84/graphics/ParamName3.3.inl>
#else
#   include <kt84/graphics/ParamName2.1.inl>
#endif
    };
    inline int    glGet1i(ParamName pname) { int    value; glGetIntegerv(static_cast<GLenum>(pname), &value); return value; }
    inline float  glGet1f(ParamName pname) { float  value; glGetFloatv  (static_cast<GLenum>(pname), &value); return value; }
    inline double glGet1d(ParamName pname) { double value; glGetDoublev (static_cast<GLenum>(pname), &value); return value; }
    template <class EigenType> inline EigenType glGetXi(ParamName pname) { EigenType value; glGetIntegerv(static_cast<GLenum>(pname), value.data()); return value; }
    template <class EigenType> inline EigenType glGetXf(ParamName pname) { EigenType value; glGetFloatv  (static_cast<GLenum>(pname), value.data()); return value; }
    template <class EigenType> inline EigenType glGetXd(ParamName pname) { EigenType value; glGetDoublev (static_cast<GLenum>(pname), value.data()); return value; }
    
    inline Eigen::Matrix4d get_modelview_matrix() {
        return glGetXd<Eigen::Matrix4d>(ParamName::MODELVIEW_MATRIX);
    }
    inline Eigen::Matrix4d get_projection_matrix() {
        return glGetXd<Eigen::Matrix4d>(ParamName::PROJECTION_MATRIX);
    }
    inline Eigen::Vector4i get_viewport() {
        return glGetXi<Eigen::Vector4i>(ParamName::VIEWPORT);
    }
    inline float read_depth(int x, int y) {
        float z = 0;
    	glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
        return z;
    }
    inline Eigen::Vector3d unproject(const Eigen::Vector3d& win_xyz,
                                     const Eigen::Matrix4d& modelview_matrix,
                                     const Eigen::Matrix4d& projection_matrix,
                                     const Eigen::Vector4i& viewport)
    {
        Eigen::Vector3d obj_xyz;
    	gluUnProject( win_xyz.x(),  win_xyz.y(),  win_xyz.z(),     modelview_matrix.data(), projection_matrix.data(), viewport.data(),
                     &obj_xyz.x(), &obj_xyz.y(), &obj_xyz.z());
        return obj_xyz;
    }
    inline Eigen::Vector3d unproject(const Eigen::Vector3d& win_xyz) {
        return unproject(win_xyz, get_modelview_matrix(), get_projection_matrix(), get_viewport());
    }
    inline bool read_and_unproject(int x, int y, Eigen::Vector3d& res) {
        auto modelview_matrix  = get_modelview_matrix();
        auto projection_matrix = get_projection_matrix();
        auto viewport          = get_viewport();
        
        float z = read_depth(x, y);
        if (z == 1.0f)
            return false;
        
        res = unproject(Eigen::Vector3d(x, y, z));
        return true;
    }
    inline Eigen::Vector3d project(const Eigen::Vector3d& obj_xyz,
                                   const Eigen::Matrix4d& modelview_matrix,
                                   const Eigen::Matrix4d& projection_matrix,
                                   const Eigen::Vector4i& viewport)
    {
        Eigen::Vector3d win_xyz;
    	gluProject( obj_xyz.x(),  obj_xyz.y(),  obj_xyz.z(),    modelview_matrix.data(), projection_matrix.data(), viewport.data(),
                   &win_xyz.x(), &win_xyz.y(), &win_xyz.z());
        return win_xyz;
    }
    inline Eigen::Vector3d project(const Eigen::Vector3d& obj_xyz) {
        return project(obj_xyz, get_modelview_matrix(), get_projection_matrix(), get_viewport());
    }
    // interface to GL functions with arbitrary data type
    // glColor
#define KT84_DEFINE_GLCOLOR(suffix) \
    template<typename T> void glColor3##suffix(T&& vec) { ::glColor3##suffix(vec[0], vec[1], vec[2]); } \
    template<typename T> void glColor4##suffix(T&& vec) { ::glColor4##suffix(vec[0], vec[1], vec[2], vec[3]); } \
    template<typename TRGB, typename TAlpha> void glColor4##suffix(TRGB&& rgb, TAlpha&& alpha) { ::glColor4##suffix(rgb[0], rgb[1], rgb[2], alpha); }
#define KT84_DEFINE_GLCOLOR_I(suffix) KT84_DEFINE_GLCOLOR(suffix) KT84_DEFINE_GLCOLOR(u##suffix)
    KT84_DEFINE_GLCOLOR(f)
    KT84_DEFINE_GLCOLOR(d)
    KT84_DEFINE_GLCOLOR_I(i)
    KT84_DEFINE_GLCOLOR_I(b)
    KT84_DEFINE_GLCOLOR_I(s)
#undef KT84_DEFINE_GLCOLOR_I
#undef KT84_DEFINE_GLCOLOR
    // glVertex
#define KT84_DEFINE_GLVERTEX(suffix) \
    template<typename T> void glVertex2##suffix(T&& vec) { ::glVertex2##suffix(vec[0], vec[1]); } \
    template<typename T> void glVertex3##suffix(T&& vec) { ::glVertex3##suffix(vec[0], vec[1], vec[2]); } \
    template<typename T> void glVertex4##suffix(T&& vec) { ::glVertex4##suffix(vec[0], vec[1], vec[2], vec[3]); }
    KT84_DEFINE_GLVERTEX(f)
    KT84_DEFINE_GLVERTEX(d)
    KT84_DEFINE_GLVERTEX(i)
    KT84_DEFINE_GLVERTEX(s)
#undef KT84_DEFINE_GLVERTEX_I
#undef KT84_DEFINE_GLVERTEX
    // glNormal
#define KT84_DEFINE_GLNORMAL(suffix) \
    template<typename T> void glNormal3##suffix(T&& vec) { ::glNormal3##suffix(vec[0], vec[1], vec[2]); }
    KT84_DEFINE_GLNORMAL(f)
    KT84_DEFINE_GLNORMAL(d)
    KT84_DEFINE_GLNORMAL(i)
    KT84_DEFINE_GLNORMAL(b)
    KT84_DEFINE_GLNORMAL(s)
#undef KT84_DEFINE_GLNORMAL
    // glTexCoord
#define KT84_DEFINE_GLTEXCOORD(suffix) \
    template<typename T> void glTexCoord2##suffix(T&& vec) { ::glTexCoord2##suffix(vec[0], vec[1]); } \
    template<typename T> void glTexCoord3##suffix(T&& vec) { ::glTexCoord3##suffix(vec[0], vec[1], vec[2]); } \
    template<typename T> void glTexCoord4##suffix(T&& vec) { ::glTexCoord4##suffix(vec[0], vec[1], vec[2], vec[3]); }
    KT84_DEFINE_GLTEXCOORD(f)
    KT84_DEFINE_GLTEXCOORD(d)
    KT84_DEFINE_GLTEXCOORD(i)
    KT84_DEFINE_GLTEXCOORD(s)
#undef KT84_DEFINE_GLTEXCOORD
    // glTranslate
    template <typename T> void glTranslated(T&& vec) { ::glTranslated(vec[0], vec[1], vec[2]); }
    template <typename T> void glTranslatef(T&& vec) { ::glTranslatef(vec[0], vec[1], vec[2]); }
    // glScale
    template <typename T> void glScaled(T&& vec) { ::glScaled(vec[0], vec[1], vec[2]); }
    template <typename T> void glScalef(T&& vec) { ::glScalef(vec[0], vec[1], vec[2]); }
    inline void glScaled(double s) { ::glScaled(s, s, s); }
    inline void glScalef(float  s) { ::glScalef(s, s, s); }
    // glRotate
    template <typename T> void glRotated(T&& angleaxis) { ::glRotated(angleaxis[0], angleaxis[1], angleaxis[2], angleaxis[3]); }
    template <typename T> void glRotatef(T&& angleaxis) { ::glRotatef(angleaxis[0], angleaxis[1], angleaxis[2], angleaxis[3]); }
    template <typename T> void glRotated(double angle, T&& axis) { ::glRotated(angle, axis[0], axis[1], axis[2]); }
    template <typename T> void glRotatef(float  angle, T&& axis) { ::glRotatef(angle, axis[0], axis[1], axis[2]); }
    // gluLookAt
    template <typename T1, typename T2, typename T3>
    void gluLookAt(T1&& eye, T2&& center, T3&& up) { ::gluLookAt(eye[0], eye[1], eye[2], center[0], center[1], center[2], up[0], up[1], up[2]); }
    // glRasterPos
#define KT84_DEFINE_GLRASTERPOS(suffix) \
    template<typename T> void glRasterPos2##suffix(T&& vec) { ::glRasterPos2##suffix(vec[0], vec[1]); } \
    template<typename T> void glRasterPos3##suffix(T&& vec) { ::glRasterPos3##suffix(vec[0], vec[1], vec[2]); } \
    template<typename T> void glRasterPos4##suffix(T&& vec) { ::glRasterPos4##suffix(vec[0], vec[1], vec[2], vec[3]); }
    KT84_DEFINE_GLRASTERPOS(s)
    KT84_DEFINE_GLRASTERPOS(i)
    KT84_DEFINE_GLRASTERPOS(f)
    KT84_DEFINE_GLRASTERPOS(d)
#undef KT84_DEFINE_GLRASTERPOS
    // glClearColor
    template <typename T> void glClearColor(T&& vec) { ::glClearColor(vec[0], vec[1], vec[2], vec[3]); }
    template<typename TRGB, typename TAlpha> void glClearColor(TRGB&& rgb, TAlpha&& alpha) { ::glClearColor(rgb[0], rgb[1], rgb[2], alpha); }

    // Lighting
    inline void glLightAmbient4f (float r, float g, float b, float a, size_t index = 0) { float v[4] = {r, g, b, a}; ::glLightfv(GL_LIGHT0 + index, GL_AMBIENT , v); }
    inline void glLightDiffuse4f (float r, float g, float b, float a, size_t index = 0) { float v[4] = {r, g, b, a}; ::glLightfv(GL_LIGHT0 + index, GL_DIFFUSE , v); }
    inline void glLightSpecular4f(float r, float g, float b, float a, size_t index = 0) { float v[4] = {r, g, b, a}; ::glLightfv(GL_LIGHT0 + index, GL_SPECULAR, v); }
    inline void glLightPosition4f(float r, float g, float b, float a, size_t index = 0) { float v[4] = {r, g, b, a}; ::glLightfv(GL_LIGHT0 + index, GL_POSITION, v); }
    template <typename T> void glLightAmbient4f (T&& vec, size_t index = 0) { ::glLightfv(GL_LIGHT0 + index, GL_AMBIENT , &vec[0]); }
    template <typename T> void glLightDiffuse4f (T&& vec, size_t index = 0) { ::glLightfv(GL_LIGHT0 + index, GL_DIFFUSE , &vec[0]); }
    template <typename T> void glLightSpecular4f(T&& vec, size_t index = 0) { ::glLightfv(GL_LIGHT0 + index, GL_SPECULAR, &vec[0]); }
    template <typename T> void glLightPosition4f(T&& vec, size_t index = 0) { ::glLightfv(GL_LIGHT0 + index, GL_POSITION, &vec[0]); }
    template <typename T> void glLightAmbient3f (T&& vec, size_t index = 0) { glLightAmbient4f (vec[0], vec[1], vec[2], 1.f, index); }
    template <typename T> void glLightDiffuse3f (T&& vec, size_t index = 0) { glLightDiffuse4f (vec[0], vec[1], vec[2], 1.f, index); }
    template <typename T> void glLightSpecular3f(T&& vec, size_t index = 0) { glLightSpecular4f(vec[0], vec[1], vec[2], 1.f, index); }
    template <typename T> void glLightPosition3f(T&& vec, size_t index = 0) { glLightPosition4f(vec[0], vec[1], vec[2], 1.f, index); }
    template <typename T> void glLightSpotDirection3f(T&& vec, size_t index = 0) { ::glLightfv(GL_LIGHT0 + index, GL_SPOT_DIRECTION, &vec[0]); }
    inline void glLightSpotExponent1f(float value, size_t index = 0) { ::glLightf(GL_LIGHT0 + index, GL_SPOT_EXPONENT, value); }
    inline void glLightSpotCutoff1f(float value, size_t index = 0) { ::glLightf(GL_LIGHT0 + index, GL_SPOT_CUTOFF, value); }
    inline void glLightConstantAttenuation1f(float value, size_t index = 0) { ::glLightf(GL_LIGHT0 + index, GL_CONSTANT_ATTENUATION, value); }
    inline void glLightLinearAttenuation1f(float value, size_t index = 0) { ::glLightf(GL_LIGHT0 + index, GL_LINEAR_ATTENUATION, value); }
    inline void glLightQuadraticAttenuation1f(float value, size_t index = 0) { ::glLightf(GL_LIGHT0 + index, GL_QUADRATIC_ATTENUATION, value); }

    // Material (face: 0->front&back, 1->front, other->back)
    inline void glMaterialAmbient4f (float r, float g, float b, float a, size_t face = 0) { float v[4] = {r, g, b, a}; ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_AMBIENT , v); }
    inline void glMaterialDiffuse4f (float r, float g, float b, float a, size_t face = 0) { float v[4] = {r, g, b, a}; ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_DIFFUSE , v); }
    inline void glMaterialSpecular4f(float r, float g, float b, float a, size_t face = 0) { float v[4] = {r, g, b, a}; ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_SPECULAR, v); }
    inline void glMaterialEmission4f(float r, float g, float b, float a, size_t face = 0) { float v[4] = {r, g, b, a}; ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_EMISSION, v); }
    template <typename T> void glMaterialAmbient4f (T&& vec, size_t face = 0) { ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_AMBIENT , &vec[0]); }
    template <typename T> void glMaterialDiffuse4f (T&& vec, size_t face = 0) { ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_DIFFUSE , &vec[0]); }
    template <typename T> void glMaterialSpecular4f(T&& vec, size_t face = 0) { ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_SPECULAR, &vec[0]); }
    template <typename T> void glMaterialEmission4f(T&& vec, size_t face = 0) { ::glMaterialfv(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_EMISSION, &vec[0]); }
    template <typename T> void glMaterialAmbient3f (T&& vec, size_t face = 0) { glMaterialAmbient4f (vec[0], vec[1], vec[2], 1.f, face); }
    template <typename T> void glMaterialDiffuse3f (T&& vec, size_t face = 0) { glMaterialDiffuse4f (vec[0], vec[1], vec[2], 1.f, face); }
    template <typename T> void glMaterialSpecular3f(T&& vec, size_t face = 0) { glMaterialSpecular4f(vec[0], vec[1], vec[2], 1.f, face); }
    template <typename T> void glMaterialEmission3f(T&& vec, size_t face = 0) { glMaterialEmission4f(vec[0], vec[1], vec[2], 1.f, face); }
    inline void glMaterialShininess1f(float value, size_t face = 0) { ::glMaterialf(face == 0 ? GL_FRONT_AND_BACK : face == 1 ? GL_FRONT : GL_BACK, GL_SHININESS, value); }

    inline GLenum glCheckError_(const char *file, int line) {
        GLenum errorCode;
        while ((errorCode = glGetError()) != GL_NO_ERROR) {
            std::string error;
            switch (errorCode) {
                case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
                case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
                case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
                case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
                case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
                case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
                case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
            }
            std::cout << error << " | " << file << " (" << line << ")" << std::endl;
        }
        return errorCode;
    }
}

}

#define glCheckError() kt84::graphics_util::glCheckError_(__FILE__, __LINE__)
