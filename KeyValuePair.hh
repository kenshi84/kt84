#pragma once

namespace kt84 {

template <typename KeyType_, typename ValueType_>
struct KeyValuePair {
	using KeyType   = KeyType_;
	using ValueType = ValueType_;
	KeyType key;
	ValueType value;
    bool operator<(const KeyValuePair& rhs) const { return key < rhs.key; }
    bool operator>(const KeyValuePair& rhs) const { return key > rhs.key; }
    bool operator<=(const KeyValuePair& rhs) const { return key <= rhs.key; }
    bool operator>=(const KeyValuePair& rhs) const { return key >= rhs.key; }
};

}
