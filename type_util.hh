#pragma once
#include <type_traits>

namespace kt84 { namespace type_util {

namespace detail {
template <bool B, class T, class F> struct Conditional;
template <class T, class F> struct Conditional<true ,T,F> { static T func(const T& trueVal, const F& falseVal) { return trueVal ; } };
template <class T, class F> struct Conditional<false,T,F> { static F func(const T& trueVal, const F& falseVal) { return falseVal; } };

}

template <bool B, class T, class F>
inline auto conditional(const T& trueVal, const F& falseVal) {
    return detail::Conditional<B,T,F>::func(trueVal, falseVal);
}

} }
