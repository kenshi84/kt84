#pragma once
#include <array>
#include <vector>
#include <string>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <kt84/detail/tinyfiledialogs.h>

namespace kt84 {
    namespace tinyfd_util {
        namespace internal {
            inline std::string& escape_quote(std::string& str) {
                for (char& c : str)
                    if (c == '\'' || c == '\"')
                        c = '-';
                return str;
            }
            inline std::string& escape_nt(std::string& str) {
                for (char& c : str)
                    if (c == '\n' || c == '\t')
                        c = ' ';
                return str;
            }
        }
        inline int messageBox(
            std::string title = "title",
            std::string message = "message", /* may contain \n and \t */
            char const * const aDialogType = "yesno", /* "ok" "okcancel" "yesno" */
            char const * const aIconType = "question", /* "info" "warning" "error" "question" */
            int const aDefaultButton = 0)  /* 0 for cancel/no , 1 for ok/yes */
        {
            char const * const aTitle = title.empty() ? nullptr : internal::escape_quote(title).c_str();
            char const * const aMessage = message.empty() ? nullptr : internal::escape_quote(message).c_str();
            /* returns 0 for cancel/no , 1 for ok/yes */
            return detail::tinyfd_messageBox(aTitle, aMessage, aDialogType, aIconType, aDefaultButton);
        }
        inline boost::optional<std::string> inputBox(
            std::string title = "title",
            std::string message = "message", /* may NOT contain \n nor \t (on windows)*/
            boost::optional<std::string> defaultInput = std::string{})  /* if boost::none it's a passwordBox */
        {
            char const * const aTitle = title.empty() ? nullptr : internal::escape_quote(title).c_str();
            char const * const aMessage = message.empty() ? nullptr : internal::escape_quote(internal::escape_nt(message)).c_str();
            char const * const aDefaultInput = defaultInput ? internal::escape_quote(*defaultInput).c_str() : nullptr;
            auto c_str = detail::tinyfd_inputBox(aTitle, aMessage, aDefaultInput);
            if (c_str)
                return std::string(c_str);
            return boost::none;
        }
        template <typename T>
        inline boost::optional<T> inputBoxT(
            std::string title = "title",
            std::string message = "message", /* may NOT contain \n nor \t (on windows)*/
            const T& aDefaultInput = T{})
        {
            char const * const aTitle = title.empty() ? nullptr : internal::escape_quote(title).c_str();
            char const * const aMessage = message.empty() ? nullptr : internal::escape_quote(internal::escape_nt(message)).c_str();
            auto c_str = detail::tinyfd_inputBox(aTitle, aMessage, boost::lexical_cast<std::string>(aDefaultInput).c_str());
            if (c_str == nullptr)
                return boost::none;
            try {
                return boost::lexical_cast<T>(c_str);
            } catch(const boost::bad_lexical_cast &) {
                return boost::none;
            }
        }
        inline boost::optional<std::string> saveFileDialog(
            std::string title = "title",
            std::string defaultPathAndFile = "",
            const std::vector<const char*>& aFilterPatterns = {"*.jpg", "*.png"},
            std::string singleFilterDescription = "image files")
        {
            char const * const aTitle = title.empty() ? nullptr : internal::escape_quote(title).c_str();
            char const * const aDefaultPathAndFile = internal::escape_quote(defaultPathAndFile).c_str();
            char const * const aSingleFilterDescription = singleFilterDescription.empty() ? nullptr : internal::escape_quote(singleFilterDescription).c_str(); /* NULL or "image files" */
            auto c_str = detail::tinyfd_saveFileDialog(aTitle, aDefaultPathAndFile, (int)aFilterPatterns.size(), aFilterPatterns.empty() ? nullptr : &aFilterPatterns[0], aSingleFilterDescription);
            if (c_str)
                return std::string(c_str);
            return boost::none;
        }
        inline boost::optional<std::string> openFileDialog(
            std::string title = "title",
            std::string defaultPathAndFile = "",
            const std::vector<const char*>& aFilterPatterns = {"*.jpg","*.png"},
            std::string singleFilterDescription = "image files",
            bool aAllowMultipleSelects = false)
            /* in case of multiple files, the separator is | */
        {
            char const * const aTitle = title.empty() ? nullptr : internal::escape_quote(title).c_str();
            char const * const aDefaultPathAndFile = internal::escape_quote(defaultPathAndFile).c_str();
            char const * const aSingleFilterDescription = singleFilterDescription.empty() ? nullptr : internal::escape_quote(singleFilterDescription).c_str();
            auto c_str = detail::tinyfd_openFileDialog(aTitle, aDefaultPathAndFile, (int)aFilterPatterns.size(), aFilterPatterns.empty() ? nullptr : &aFilterPatterns[0], aSingleFilterDescription, aAllowMultipleSelects);
            if (c_str)
                return std::string(c_str);
            return boost::none;
        }
        inline boost::optional<std::string> selectFolderDialog(
            std::string title = "title",
            std::string defaultPath = "")
        {
            char const * const aTitle = title.empty() ? nullptr : internal::escape_quote(title).c_str();
            char const * const aDefaultPath = internal::escape_quote(defaultPath).c_str();
            auto c_str = detail::tinyfd_selectFolderDialog(aTitle, aDefaultPath);
            if (c_str)
                return std::string(c_str);
            return boost::none;
        }
        inline boost::optional<std::pair<std::string, std::array<unsigned char, 3>>> colorChooser(
            std::string title = "title",
            char const * const aDefaultHexRGB = "#FF0000", /* NULL or "#FF0000" */
            const std::array<unsigned char, 3>& aDefaultRGB = {0,255,255})
	        /* returns color in hex-string and RGB */
	        /* aDefaultRGB is used only if aDefaultHexRGB is NULL */
        {
            char const * const aTitle = title.empty() ? nullptr : internal::escape_quote(title).c_str();
            std::array<unsigned char, 3> aoResultRGB;
            auto c_str = detail::tinyfd_colorChooser(aTitle, aDefaultHexRGB, aDefaultRGB.data(), aoResultRGB.data());
            if (c_str)
                return std::make_pair(std::string(c_str), aoResultRGB);
            return boost::none;
        }
    }
}
