#pragma once
#include <iterator>
#include <tbb/parallel_for.h>

namespace kt84 { namespace tbb_util {

template <typename Range, typename Func>
void parallel_for(Range& range, Func func, size_t grainsize = 1024) {
    auto begin_ = std::begin(range), end_ = std::end(range);
    using Value = decltype(begin_);
    tbb::parallel_for(
        tbb::blocked_range<Value>(begin_, end_, grainsize),
        [&func](const tbb::blocked_range<Value> &range_)
    {
        for (auto& elem : range_)
            func(elem);
    });
}

template <typename Func>
void parallel_for(size_t size, Func func, size_t grainsize = 1024) {
    tbb::parallel_for(
        tbb::blocked_range<size_t>(0u, size, grainsize),
        [&func](const tbb::blocked_range<size_t> &range)
    {
        for (auto i = range.begin(); i != range.end(); ++i)
            func(i);
    });
}

}}
