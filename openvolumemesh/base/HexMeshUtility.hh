#pragma once
#include <utility>
#include <OpenVolumeMesh/Mesh/HexahedralMesh.hh>
#include <kt84/openvolumemesh/base/Utility.hh>

namespace kt84 { namespace openvolumemesh {

namespace internal {

struct HalfFaceIncidence_Hex {
    std::vector<HFHandle> halffaces;
};
struct CellIncidence_Hex {
    std::array<std::vector<CHandle>,3> cells;
};

}

template <class Base>
struct HexMeshUtility : public Utility<Base> {
    void utility_storeIncidence() {
        Utility<Base>::utility_storeIncidence();
        // halffaces
        halffaceIncidence_hex.clear();
        halffaceIncidence_hex.resize(Base::n_halffaces());
        for (auto hf : Base::halffaces()) {
            if (Base::is_boundary(hf))
                continue;
            auto& hf_incidence = halffaceIncidence_hex[hf];
            for (auto hf2 : Base::halfface_sheet_halffaces(hf))
                hf_incidence.halffaces.push_back(hf2);
        }
        // cell
        cellIncidence_hex.clear();
        cellIncidence_hex.resize(Base::n_cells());
        for (auto c : Base::cells()) {
            auto& c_incidence = Utility<Base>::cellIncidence[c];
            c_incidence.vertices.clear();
            for (auto v : Base::hex_vertices(c))
                c_incidence.vertices.push_back(v);
            auto& c_incidence_hex = cellIncidence_hex[c];
            for (auto c2 : Base::cell_sheet_cells(c, OpenVolumeMesh::HexahedralMeshTopologyKernel::XF))
                c_incidence_hex.cells[0].push_back(c2);
            for (auto c2 : Base::cell_sheet_cells(c, OpenVolumeMesh::HexahedralMeshTopologyKernel::YF))
                c_incidence_hex.cells[1].push_back(c2);
            for (auto c2 : Base::cell_sheet_cells(c, OpenVolumeMesh::HexahedralMeshTopologyKernel::ZF))
                c_incidence_hex.cells[2].push_back(c2);
        }
    }
    auto& halfface_sheet_halffaces(HFHandle hf) const { return halffaceIncidence_hex[hf].halffaces; }
    auto& cell_sheet_cells(CHandle c, unsigned char orthDir) const {
        int axis = -1;
        if (orthDir==OpenVolumeMesh::HexahedralMeshTopologyKernel::XF) axis = 0;
        if (orthDir==OpenVolumeMesh::HexahedralMeshTopologyKernel::YF) axis = 1;
        if (orthDir==OpenVolumeMesh::HexahedralMeshTopologyKernel::ZF) axis = 2;
        if (orthDir==OpenVolumeMesh::HexahedralMeshTopologyKernel::XB) axis = 0;
        if (orthDir==OpenVolumeMesh::HexahedralMeshTopologyKernel::YB) axis = 1;
        if (orthDir==OpenVolumeMesh::HexahedralMeshTopologyKernel::ZB) axis = 2;
        return cellIncidence_hex[c].cells[axis];
    }
    HFHandle opposite_halfface_handle_in_cell(HFHandle hf) const {
        if (Base::is_boundary(hf))
            return HFHandle{};
        return Base::opposite_halfface_handle_in_cell(hf);
    }
    // |   input: v, hf
    // |   output: w, he
    // |
    // |      (w)--------*
    // |      /         /|
    // |   (he)        / |
    // |    /         /  |
    // |  (v)--------*   |
    // |   |         |   *
    // |   |         |  / 
    // |   |  (hf)   | /  
    // |   |         |/
    // |   *---------*
    std::pair<VHandle, HEHandle> opposite_vertex_handle_in_cell(VHandle v, HFHandle hf) const {
        if (Base::is_boundary(hf))
            return {};
        auto hf2 = opposite_halfface_handle_in_cell(hf);
        for (auto w : Base::halfface_vertices(hf2)) {
            auto he = Base::halfedge(v, w);
            if (he.is_valid())
                return { w, he };
        }
        return {};  // shouldn't reach here
    }
    std::array<HEHandle,3> hex_halfedge_parallel_halfedges(HEHandle he, CHandle c) const {
        // |   input: he, c
        // |   output: he0, he1, he2
        // |
        // |       ^---------^
        // |      /|        /|
        // |     /(he2)    / |
        // |    /  |      / (he1)
        // |   ^---------^   |
        // |   |   *-----|---*
        // | (he) /    (he0)/ 
        // |   | /       | /  
        // |   |/        |/
        // |   *---------*
        assert(Utility<Base>::is_incident(c, he));
        auto v = Utility<Base>::halfedge_vertices(he);
        HFHandle hf_bottom, hf_top;
        for (auto hf : Utility<Base>::cell_halffaces(c)) {
            if (Utility<Base>::is_incident(hf, v[0]) && !Utility<Base>::is_incident(hf, v[1])) {
                assert(!hf_bottom.is_valid());
                hf_bottom = hf;
            }
            if (!Utility<Base>::is_incident(hf, v[0]) && Utility<Base>::is_incident(hf, v[1])) {
                assert(!hf_top.is_valid());
                hf_top = hf;
            }
        }
        assert(hf_bottom.is_valid());
        assert(hf_top   .is_valid());
        std::vector<VHandle> v_bottom = Utility<Base>::halfface_vertices(hf_bottom),
                             v_top    = Utility<Base>::halfface_vertices(hf_top   );
        container_util::bring_front(v_bottom, v[0]);
        container_util::bring_front(v_top   , v[1]);
        auto he0 = Base::halfedge(v_bottom[1], v_top[3]),
             he1 = Base::halfedge(v_bottom[2], v_top[2]),
             he2 = Base::halfedge(v_bottom[3], v_top[1]);
        assert(he0.is_valid());
        assert(he1.is_valid());
        assert(he2.is_valid());
        return { he0, he1, he2 };
    }
    std::array<EHandle,3> hex_edge_parallel_edges(EHandle e, CHandle c) const {
        HEHandle he = Base::halfedge_handle(e, 0);
        std::array<HEHandle,3> he012 = hex_halfedge_parallel_halfedges(he, c);
        std::array<EHandle,3> e012;
        for (int i = 0; i < 3; ++i)
            e012[i] = Base::edge_handle(he012[i]);
        return e012;
    }
protected:
    std::vector<internal::HalfFaceIncidence_Hex> halffaceIncidence_hex;
    std::vector<internal::CellIncidence_Hex> cellIncidence_hex;
};

} }
