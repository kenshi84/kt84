#pragma once
#include <array>
#include <vector>
#include <OpenVolumeMesh/Core/OpenVolumeMeshHandle.hh>
#include <kt84/container_util.hh>

namespace kt84 { namespace openvolumemesh {

using VHandle = OpenVolumeMesh::VertexHandle;
using EHandle = OpenVolumeMesh::EdgeHandle;
using FHandle = OpenVolumeMesh::FaceHandle;
using CHandle = OpenVolumeMesh::CellHandle;
using HEHandle = OpenVolumeMesh::HalfEdgeHandle;
using HFHandle = OpenVolumeMesh::HalfFaceHandle;

namespace internal {
struct VertexIncidence {
    std::vector<VHandle> vertices;
    std::vector<EHandle> edges;
    std::vector<HEHandle> outgoing_halfedges,
                          incoming_halfedges;
    std::vector<FHandle> faces;
    std::vector<CHandle> cells;
    std::vector<VHandle> boundary_vertices;
    std::vector<EHandle> boundary_edges;
    std::vector<HEHandle> boundary_outgoing_halfedges,
                          boundary_incoming_halfedges;
    std::vector<HFHandle> boundary_halffaces;
    int boundary_idx = -1;
};
struct HalfEdgeIncidence {
    VHandle from_vertex, to_vertex;
    std::array<VHandle,2> vertices;        // { from_vertex, to_vertex }
    std::vector<HFHandle> halffaces;
    std::vector<CHandle> cells;
    HFHandle boundary_halfface;
    int boundary_idx = -1;
};
struct EdgeIncidence {
    std::vector<FHandle> faces;
    std::array<FHandle,2> boundary_faces;
    std::array<HFHandle,2> boundary_halffaces;
    int boundary_idx = -1;
};
struct HalfFaceIncidence {
    std::vector<VHandle> vertices;
    std::vector<HEHandle> halfedges;
    std::vector<HFHandle> boundary_halffaces;
    CHandle cell;
    int boundary_idx = -1;
};
struct FaceIncidence {
    std::vector<EHandle> edges;
    std::vector<FHandle> boundary_faces;
    std::array<CHandle,2> cells;
    int boundary_idx = -1;
};
struct CellIncidence {
    std::vector<VHandle> vertices;
    std::vector<EHandle> edges;
    std::vector<FHandle> faces;
    std::vector<HFHandle> halffaces;
    std::vector<CHandle> cells;
};

template <class HandleT>
inline void make_start_with_lowest_idx(std::vector<HandleT>& vec) {
    auto pos = vec.begin();
    for (auto i = vec.begin()+1; i != vec.end(); ++i) {
        if (i->idx() < pos->idx())
            pos = i;
    }
    boost::rotate(vec, pos);
}

}
template <class Base>
struct Utility : public Base {
    void utility_storeIncidence() {
        // vertex
        vertexIncidence.clear();
        vertexIncidence.resize(Base::n_vertices());
        for (auto v : Base::vertices()) {
            auto& v_incidence = vertexIncidence[v];
            for (auto he : Base::outgoing_halfedges(v)) {
                v_incidence.vertices.push_back(Base::halfedge(he).to_vertex());
                v_incidence.edges.push_back(Base::edge_handle(he));
                v_incidence.outgoing_halfedges.push_back(he);
                v_incidence.incoming_halfedges.push_back(Base::opposite_halfedge_handle(he));
                for (auto hf : Base::halfedge_halffaces(he))
                    v_incidence.faces.push_back(Base::face_handle(hf));
            }
            container_util::remove_duplicate(v_incidence.faces);
            if (Base::is_boundary(v)) {
                for (auto f : v_incidence.faces)
                    if (Base::is_boundary(f))
                        v_incidence.boundary_halffaces.push_back(boundary_halfface(f));
                internal::make_start_with_lowest_idx(v_incidence.boundary_halffaces);
                auto unsorted = v_incidence.boundary_halffaces;
                v_incidence.boundary_halffaces = { unsorted.back() };
                unsorted.pop_back();
                while (true) {
                    auto hf = v_incidence.boundary_halffaces.back();
                    auto halfedges = Base::halfface(hf).halfedges();
                    auto he = halfedges.begin();
                    for (; ; ++he)
                        if (Base::halfedge(*he).to_vertex() == v)
                            break;
                    v_incidence.boundary_incoming_halfedges.push_back(*he);
                    if (unsorted.empty())
                        break;
                    bool found = false;
                    for (auto hf2=unsorted.begin(); hf2!=unsorted.end(); ++hf2) {
                        auto halfedges2 = Base::halfface(*hf2).halfedges();
                        for (auto he2 : halfedges2) {
                            if (Base::opposite_halfedge_handle(*he)==he2) {
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            v_incidence.boundary_halffaces.push_back(*hf2);
                            unsorted.erase(hf2);
                            break;
                        }
                    }
                    if (!found) {
                        std::cout << "warning: detected non-manifold faces at the boundary!" << std::endl;
                        break;
                    }
                }
                for (auto he : v_incidence.boundary_incoming_halfedges) {
                    v_incidence.boundary_vertices.push_back(Base::halfedge(he).from_vertex());
                    v_incidence.boundary_outgoing_halfedges.push_back(Base::opposite_halfedge_handle(he));
                    v_incidence.boundary_edges.push_back(Base::edge_handle(he));
                }
            }
            for (auto c : Base::vertex_cells(v))
                v_incidence.cells.push_back(c);
        }
        // edge
        edgeIncidence.clear();
        edgeIncidence.resize(Base::n_edges());
        halfedgeIncidence.clear();
        halfedgeIncidence.resize(Base::n_halfedges());
        for (auto e : Base::edges()) {
            auto he0 = Base::halfedge_handle(e,0),
                 he1 = Base::halfedge_handle(e,1);
            auto& e_incidence = edgeIncidence[e];
            auto &he0_incidence = halfedgeIncidence[he0],
                 &he1_incidence = halfedgeIncidence[he1];
            he0_incidence.to_vertex = he1_incidence.from_vertex = Base::halfedge(he0).to_vertex();
            he0_incidence.from_vertex = he1_incidence.to_vertex = Base::halfedge(he0).from_vertex();
            he0_incidence.vertices = { he0_incidence.from_vertex, he0_incidence.to_vertex };
            he1_incidence.vertices = { he1_incidence.from_vertex, he1_incidence.to_vertex };
            for (auto hf0 : Base::halfedge_halffaces(he0)) {
                he0_incidence.halffaces.push_back(hf0);
            }
            for (auto hf1 : Base::halfedge_halffaces(he1)) {
                he1_incidence.halffaces.push_back(hf1);
            }
            if (!Base::is_boundary(e)) {
                internal::make_start_with_lowest_idx(he0_incidence.halffaces);
                internal::make_start_with_lowest_idx(he1_incidence.halffaces);
            } else {
                // assert(Base::is_boundary(he0_incidence.halffaces.back()));
                // assert(Base::is_boundary(he1_incidence.halffaces.back()));
            }
            for (auto hf0 : he0_incidence.halffaces) {
                e_incidence.faces.push_back(Base::face_handle(hf0));
            }
            for (auto hf0 : he0_incidence.halffaces) {
                if (!Base::is_boundary(hf0))
                    he0_incidence.cells.push_back(Base::incident_cell(hf0));
            }
            for (auto hf1 : he1_incidence.halffaces) {
                if (!Base::is_boundary(hf1))
                    he1_incidence.cells.push_back(Base::incident_cell(hf1));
            }
        }
        // face
        faceIncidence.clear();
        faceIncidence.resize(Base::n_faces());
        halffaceIncidence.clear();
        halffaceIncidence.resize(Base::n_halffaces());
        for (auto f : Base::faces()) {
            auto hf0 = Base::halfface_handle(f,0),
                 hf1 = Base::halfface_handle(f,1);
            auto& f_incidence = faceIncidence[f];
            auto &hf0_incidence = halffaceIncidence[hf0],
                 &hf1_incidence = halffaceIncidence[hf1];
            hf0_incidence.halfedges = Base::halfface(hf0).halfedges();
            hf1_incidence.halfedges = Base::halfface(hf1).halfedges();
            for (auto he : hf0_incidence.halfedges)
                f_incidence.edges.push_back(Base::edge_handle(he));
            for (auto v : Base::halfface_vertices(hf0))
                hf0_incidence.vertices.push_back(v);
            for (auto v : Base::halfface_vertices(hf1))
                hf1_incidence.vertices.push_back(v);
            if (Base::is_boundary(f)) {
                auto hf_boundary = boundary_halfface_handle(f);
                for (auto hf2 : Base::boundary_halfface_halffaces(hf_boundary)) {
                    halffaceIncidence[hf_boundary].boundary_halffaces.push_back(hf2);
                    f_incidence.boundary_faces.push_back(Base::face_handle(hf2));
                }
            }
            hf0_incidence.cell = Base::incident_cell(hf0);
            hf1_incidence.cell = Base::incident_cell(hf1);
            f_incidence.cells = {hf0_incidence.cell, hf1_incidence.cell};
            if (!f_incidence.cells[0].is_valid())
                std::swap(f_incidence.cells[0], f_incidence.cells[1]);
        }
        // cell
        cellIncidence.clear();
        cellIncidence.resize(Base::n_cells());
        for (auto c : Base::cells()) {
            auto& c_incidence = cellIncidence[c];
            for (auto v : Base::cell_vertices(c))
                c_incidence.vertices.push_back(v);
            c_incidence.halffaces = Base::cell(c).halffaces();
            for (auto hf : c_incidence.halffaces) {
                auto hf_halfedges = Base::halfface(hf).halfedges();
                for (auto he : hf_halfedges)
                    c_incidence.edges.push_back(Base::edge_handle(he));
                c_incidence.faces.push_back(Base::face_handle(hf));
            }
            container_util::remove_duplicate(c_incidence.edges);
            for (auto c2 : Base::cell_cells(c))
                c_incidence.cells.push_back(c2);
        }
        // boundary elements
        boundary_vertices_.clear();
        boundary_edges_.clear();
        boundary_faces_.clear();
        boundary_halfedges_.clear();
        boundary_halffaces_.clear();
        for (auto v : Base::vertices()) {
            if (!Base::is_boundary(v))
                continue;
            vertexIncidence[v].boundary_idx = boundary_vertices_.size();
            boundary_vertices_.push_back(v);
        }
        for (auto e : Base::edges()) {
            if (!Base::is_boundary(e))
                continue;
            edgeIncidence[e].boundary_idx = boundary_edges_.size();
            boundary_edges_.push_back(e);
            auto& boundary_faces = edgeIncidence[e].boundary_faces;
            for (auto f : edge_faces(e)) {
                if (Base::is_boundary(f)) {
                    if (boundary_faces[0].is_valid())
                        boundary_faces[1] = f;
                    else
                        boundary_faces[0] = f;
                }
            }
            if (!boundary_faces[0].is_valid() || !boundary_faces[1].is_valid()) {
                std::cout << "warning: something wrong with boundary edge " << e << std::endl;
                continue;
            }
            for (int i=0; i<2; ++i)
                edgeIncidence[e].boundary_halffaces[i] = boundary_halfface(edgeIncidence[e].boundary_faces[i]);
        }
        for (auto f : Base::faces()) {
            if (!Base::is_boundary(f))
                continue;
            faceIncidence[f].boundary_idx = boundary_faces_.size();
            boundary_faces_.push_back(f);
        }
        for (auto he : Base::halfedges()) {
            if (!Base::is_boundary(he))
                continue;
            halfedgeIncidence[he].boundary_idx = boundary_halfedges_.size();
            boundary_halfedges_.push_back(he);
            for (auto hf : halfedge_halffaces(he)) {
                if (Base::is_boundary(hf)) {
                    halfedgeIncidence[he].boundary_halfface = hf;
                    break;
                }
            }
        }
        for (auto hf : Base::halffaces()) {
            if (!Base::is_boundary(hf))
                continue;
            halffaceIncidence[hf].boundary_idx = boundary_halffaces_.size();
            boundary_halffaces_.push_back(hf);
        }
    }
    // vertex incidence
    auto& vertex_vertices(VHandle v) const { return vertexIncidence[v].vertices; }
    auto& vertex_edges(VHandle v) const { return vertexIncidence[v].edges; }
    auto& vertex_outgoing_halfedges(VHandle v) const { return vertexIncidence[v].outgoing_halfedges; }
    auto& vertex_incoming_halfedges(VHandle v) const { return vertexIncidence[v].incoming_halfedges; }
    auto& vertex_faces(VHandle v) const { return vertexIncidence[v].faces; }
    auto& vertex_cells(VHandle v) const { return vertexIncidence[v].cells; }
    auto& boundary_vertex_vertices(VHandle v) const { return vertexIncidence[v].boundary_vertices; }
    auto& boundary_vertex_edges(VHandle v) const { return vertexIncidence[v].boundary_edges; }
    auto& boundary_vertex_outgoing_halfedges(VHandle v) const { return vertexIncidence[v].boundary_outgoing_halfedges; }
    auto& boundary_vertex_incoming_halfedges(VHandle v) const { return vertexIncidence[v].boundary_incoming_halfedges; }
    auto& boundary_vertex_halffaces(VHandle v) const { return vertexIncidence[v].boundary_halffaces; }
    // edge incidence
    auto& edge_vertices(EHandle e) const { return halfedgeIncidence[Base::halfedge_handle(e,0)].vertices; }
    auto& edge_faces(EHandle e) const { return edgeIncidence[e].faces; }
    auto& edge_cells(EHandle e) const { return halfedgeIncidence[Base::halfedge_handle(e,0)].cells; }
    auto& boundary_edge_faces(EHandle e) const { return edgeIncidence[e].boundary_faces; }
    auto& boundary_edge_halffaces(EHandle e) const { return edgeIncidence[e].boundary_halffaces; }
    // halfedge incidence
    auto& from_vertex(HEHandle he) const { return halfedgeIncidence[he].from_vertex; }
    auto& to_vertex(HEHandle he) const { return halfedgeIncidence[he].to_vertex; }
    auto& halfedge_vertices(HEHandle he) const { return halfedgeIncidence[he].vertices; }
    auto& halfedge_halffaces(HEHandle he) const { return halfedgeIncidence[he].halffaces; }
    auto& halfedge_faces(HEHandle he) const { return edgeIncidence[Base::edge_handle(he)].faces; }
    auto& halfedge_cells(HEHandle he) const { return halfedgeIncidence[he].cells; }
    auto& boundary_halfedge_halfface(HEHandle he) const { return halfedgeIncidence[he].boundary_halfface; }
    // face incidence
    auto& face_vertices(FHandle f) const { return halffaceIncidence[Base::halfface_handle(f,0)].vertices; }
    auto& face_edges(FHandle f) const { return faceIncidence[f].edges; }
    auto& face_cells(FHandle f) const { return faceIncidence[f].cells; }
    auto& boundary_face_faces(FHandle f) const { return faceIncidence[f].boundary_faces; }
    auto& boundary_face_halffaces(FHandle f) const { return halffaceIncidence[boundary_halfface_handle(f)].boundary_halffaces; }
    // halfface incidence
    auto& halfface_vertices(HFHandle hf) const { return halffaceIncidence[hf].vertices; }
    auto& halfface_halfedges(HFHandle hf) const { return halffaceIncidence[hf].halfedges; }
    auto& halfface_edges(HFHandle hf) const { return faceIncidence[Base::face_handle(hf)].edges; }
    auto& halfface_cell(HFHandle hf) const { return faceIncidence[Base::face_handle(hf)].cell; }
    auto& boundary_halfface_faces(HFHandle hf) const { return faceIncidence[Base::face_handle(hf)].boundary_faces; }
    auto& boundary_halfface_halffaces(HFHandle hf) const { return halffaceIncidence[hf].boundary_halffaces; }
    // cell incidence
    auto& cell_vertices(CHandle c) const { return cellIncidence[c].vertices; }
    auto& cell_edges(CHandle c) const { return cellIncidence[c].edges; }
    auto& cell_faces(CHandle c) const { return cellIncidence[c].faces; }
    auto& cell_halffaces(CHandle c) const { return cellIncidence[c].halffaces; }
    auto& cell_cells(CHandle c) const { return cellIncidence[c].cells; }
    // boundary elements & info
    auto& boundary_vertex_idx(VHandle v) const { return vertexIncidence[v].boundary_idx; }
    auto& boundary_edge_idx(EHandle e) const { return edgeIncidence[e].boundary_idx; }
    auto& boundary_face_idx(FHandle f) const { return faceIncidence[f].boundary_idx; }
    auto& boundary_halfedge_idx(HEHandle he) const { return halfedgeIncidence[he].boundary_idx; }
    auto& boundary_halfface_idx(HFHandle hf) const { return halffaceIncidence[hf].boundary_idx; }
    auto& boundary_vertices() const { return boundary_vertices_; }
    auto& boundary_edges() const { return boundary_edges_; }
    auto& boundary_faces() const { return boundary_faces_; }
    auto& boundary_halfedges() const { return boundary_halfedges_; }
    auto& boundary_halffaces() const { return boundary_halffaces_; }
    int n_boundary_vertices() const { return boundary_vertices_.size(); }
    int n_boundary_edges() const { return boundary_edges_.size(); }
    int n_boundary_faces() const { return boundary_faces_.size(); }
    int n_boundary_halfedges() const { return boundary_halfedges_.size(); }
    int n_boundary_halffaces() const { return boundary_halffaces_.size(); }
    CHandle cell_handle(HFHandle hf) const { return Base::incident_cell(hf); }
    HFHandle boundary_halfface(FHandle f) const {
        for (int i=0; i<2; ++i) {
            auto hf = Base::halfface_handle(f,i);
            if (Base::is_boundary(hf))
                return hf;
        }
        return HFHandle{};
    }
    // some extensions
    HFHandle boundary_halfface_handle(FHandle f) const {
        auto hf = Base::halfface_handle(f,0);
        if (!Base::is_boundary(hf))
            hf = Base::halfface_handle(f,1);
        return hf;
    }
    std::vector<HFHandle> halffaces_incident_to_cell_corner(CHandle c, VHandle v) const {
        std::vector<HFHandle> result;
        for (auto hf : cell_halffaces(c)) {
            bool is_incident = false;
            for (auto w : halfface_vertices(hf)) {
                if (v == w) {
                    is_incident = true;
                    break;
                }
            }
            if (is_incident)
                result.push_back(hf);
        }
        return result;
    }
    std::vector<HEHandle> outgoing_halfedges_in_cell(CHandle c, VHandle v) const {
        std::vector<HEHandle> result;
        for (auto he : vertex_outgoing_halfedges(v)) {
            for (auto c2 : halfedge_cells(he)) {
                if (c == c2) {
                    result.push_back(he);
                    break;
                }
            }
        }
        return result;
    }
    // incidence test ---------------------------
    // from halfedge
    bool is_incident(HEHandle he, VHandle v) const {
        auto w = halfedge_vertices(he);
        return w[0]==v || w[1]==v;
    }
    // from edge
    bool is_incident(EHandle e, VHandle v) const {
        auto w = edge_vertices(e);
        return w[0]==v || w[1]==v;
    }
    // from halfface
    bool is_incident(HFHandle hf, VHandle v) const {
        for (auto v2 : halfface_vertices(hf))
            if (v == v2)
                return true;
        return false;
    }
    bool is_incident(HFHandle hf, HEHandle he) const {
        for (auto he2 : halfface_halfedges(hf))
            if (he == he2)
                return true;
        return false;
    }
    bool is_incident(HFHandle hf, EHandle e) const {
        for (auto e2 : halfface_edges(hf))
            if (e == e2)
                return true;
        return false;
    }
    // from face
    bool is_incident(FHandle f, VHandle v) const {
        auto hf = Base::halfface_handle(f, 0);
        return is_incident(hf, v);
    }
    bool is_incident(FHandle f, HEHandle he) const {
        for (int i = 0; i < 2; ++i)
            if (is_incident(Base::halfface_handle(f, i), he))
                return true;
        return false;
    }
    bool is_incident(FHandle f, EHandle e) const {
        return is_incident(f, Base::halfedge_handle(e, 0));
    }
    // from cell
    bool is_incident(CHandle c, VHandle v) const {
        for (auto v2 : cell_vertices(c))
            if (v == v2)
                return true;
        return false;
    }
    bool is_incident(CHandle c, HEHandle he) const {
        for (auto hf : cell_halffaces(c))
            for (auto he2 : halfface_halfedges(hf))
                if (he == he2)
                    return true;
        return false;
    }
    bool is_incident(CHandle c, EHandle e) const {
        return is_incident(c, Base::halfedge_handle(e, 0));
    }
    bool is_incident(CHandle c, HFHandle hf) const {
        return c == Base::incident_cell(hf);
    }
    bool is_incident(CHandle c, FHandle f) const {
        for (int i = 0; i < 2; ++i)
            if (is_incident(c, Base::halfface_handle(f, i)))
                return true;
        return false;
    }
    // adjacency query -----------------
    // vertex-vertex adjacency: true if an edge exists
    bool is_adjacent(VHandle v1, VHandle v2) const {
        return Base::halfedge(v1, v2).is_valid();
    }
    // edge-edge adjacency: true if two edges share an endpoint
    bool is_adjacent(EHandle e1, EHandle e2) const {
        auto v1 = edge_vertices(e1),
             v2 = edge_vertices(e2);
        return v1[0]==v2[0] || v1[0]==v2[1] || v1[1]==v2[0] || v1[1]==v2[1];
    }
    // edge-face adjacency: true if an edge and a face share one vertex
    bool is_adjacent(EHandle e, FHandle f) const {
        for (auto e2 : face_edges(f))
            if (is_adjacent(e, e2))
                return true;
        return false;
    }
    // edge-cell adjacency: true if an edge and a cell share one vertex
    bool is_adjacent(EHandle e, CHandle c) const {
        for (auto e2 : cell_edges(c))
            if (is_adjacent(e, e2))
                return true;
        return false;
    }
    // face-face adjacency: true if two faces share one vertex
    bool is_adjacent_by_vertex(FHandle f1, FHandle f2) const {
        for (auto v1 : face_vertices(f1))
            for (auto v2 : face_vertices(f2))
                if (v1 == v2)
                    return true;
        return false;
    }
    // face-face adjacency: true if two faces share one edge
    bool is_adjacent_by_edge(FHandle f1, FHandle f2) const {
        for (auto e1 : face_edges(f1))
            for (auto e2 : face_edges(f2))
                if (e1 == e2)
                    return true;
        return false;
    }
    // face-cell adjacency: true if a face and a cell share one vertex
    bool is_adjacent_by_vertex(FHandle f, CHandle c) const {
        for (auto v1 : face_vertices(f))
            for (auto v2 : cell_vertices(c))
                if (v1 == v2)
                    return true;
        return false;
    }
    // face-cell adjacency: true if a face and a cell share one edge
    bool is_adjacent_by_edge(FHandle f, CHandle c) const {
        for (auto e : face_edges(f))
            for (auto c2 : halfedge_cells(Base::halfedge_handle(e, 0)))
                if (c == c2)
                    return true;
        return false;
    }
    // cell-cell adjacency: true if two cells share one vertex
    bool is_adjacent_by_vertex(CHandle c1, CHandle c2) const {
        for (auto v1 : cell_vertices(c1))
            for (auto v2 : cell_vertices(c2))
                if (v1 == v2)
                    return true;
        return false;
    }
    // cell-cell adjacency: true if two cells share one edge
    bool is_adjacent_by_edge(CHandle c1, CHandle c2) const {
        for (auto e1 : cell_edges(c1))
            for (auto e2 : cell_edges(c2))
                if (e1 == e2)
                    return true;
        return false;
    }
    // cell-cell adjacency: true if two cells share one face
    bool is_adjacent_by_face(CHandle c1, CHandle c2) const {
        for (auto c3 : cell_cells(c1))
            if (c2 == c3)
                return true;
        return false;
    }
    // adjacent elements within cell
    std::vector<HFHandle> adjacent_halffaces_in_cell(CHandle c, EHandle e) const {
        HFHandle hf0, hf1;
        for (auto hf : cell_halffaces(c)) {
            if (is_incident(hf, e)) {
                if (hf0.is_valid()) {
                    hf1 = hf;
                    break;
                } else {
                    hf0 = hf;
                }
            }
        }
        return { hf0, hf1 };
    }
    std::vector<FHandle> adjacent_faces_in_cell(CHandle c, EHandle e) const {
        auto p = adjacent_halffaces_in_cell(c, e);
        return { Base::face_handle(p[0]), Base::face_handle(p[1]) };
    }
    CHandle opposite_cell_handle(CHandle c, FHandle f) const {
        for (int i = 0; i < 2; ++i) {
            if (cell_handle(Base::halfface_handle(f, i)) == c)
                return cell_handle(Base::halfface_handle(f, (i+1)%2));
        }
        assert(false);
        return CHandle{};
    }
    // common elements ---------------------------------------
    VHandle common_vertex(EHandle e1, EHandle e2) const {
        for (auto v1 : edge_vertices(e1))
        for (auto v2 : edge_vertices(e2))
        {
            if (v1==v2) return v1;
        }
        return VHandle{};
    }
    EHandle common_edge(FHandle f1, FHandle f2) const {
        for (auto e1 : face_edges(f1))
        for (auto e2 : face_edges(f2))
        {
            if (e1==e2) return e1;
        }
        return EHandle{};
    }
    FHandle common_face(CHandle c1, CHandle c2) const {
        for (auto f1 : cell_faces(c1))
        for (auto f2 : cell_faces(c2))
        {
            if (f1==f2) return f1;
        }
        return FHandle{};
    }
protected:
    std::vector<internal::VertexIncidence> vertexIncidence;
    std::vector<internal::EdgeIncidence> edgeIncidence;
    std::vector<internal::HalfEdgeIncidence> halfedgeIncidence;
    std::vector<internal::FaceIncidence> faceIncidence;
    std::vector<internal::HalfFaceIncidence> halffaceIncidence;
    std::vector<internal::CellIncidence> cellIncidence;
    std::vector<VHandle> boundary_vertices_;
    std::vector<EHandle> boundary_edges_;
    std::vector<FHandle> boundary_faces_;
    std::vector<HEHandle> boundary_halfedges_;
    std::vector<HFHandle> boundary_halffaces_;
};

} }   // namespace kt84::openvolumemesh
