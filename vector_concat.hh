#pragma once
#include <type_traits>

namespace kt84 {
    namespace internal {
        // to achieve partial specialization on function template
        template <typename T_out, int N_head, int N_tail, typename T_head, typename T_tail> struct VectorConcat;
        template <typename T_out, typename T_head, typename T_tail> struct VectorConcat<T_out, 1, 2, T_head, T_tail> { static T_out impl(const T_head& head, const T_tail& tail) { static T_out dummy; using OutScalar = std::remove_reference_t<decltype(dummy[0])>; return { (OutScalar)head   , (OutScalar)tail[0], (OutScalar)tail[1]};                     } };
        template <typename T_out, typename T_head, typename T_tail> struct VectorConcat<T_out, 1, 3, T_head, T_tail> { static T_out impl(const T_head& head, const T_tail& tail) { static T_out dummy; using OutScalar = std::remove_reference_t<decltype(dummy[0])>; return { (OutScalar)head   , (OutScalar)tail[0], (OutScalar)tail[1], (OutScalar)tail[2] }; } };
        template <typename T_out, typename T_head, typename T_tail> struct VectorConcat<T_out, 2, 1, T_head, T_tail> { static T_out impl(const T_head& head, const T_tail& tail) { static T_out dummy; using OutScalar = std::remove_reference_t<decltype(dummy[0])>; return { (OutScalar)head[0], (OutScalar)head[1], (OutScalar)tail   };                     } };
        template <typename T_out, typename T_head, typename T_tail> struct VectorConcat<T_out, 3, 1, T_head, T_tail> { static T_out impl(const T_head& head, const T_tail& tail) { static T_out dummy; using OutScalar = std::remove_reference_t<decltype(dummy[0])>; return { (OutScalar)head[0], (OutScalar)head[1], (OutScalar)head[2], (OutScalar)tail    }; } };
        template <typename T_out, typename T_head, typename T_tail> struct VectorConcat<T_out, 2, 2, T_head, T_tail> { static T_out impl(const T_head& head, const T_tail& tail) { static T_out dummy; using OutScalar = std::remove_reference_t<decltype(dummy[0])>; return { (OutScalar)head[0], (OutScalar)head[1], (OutScalar)tail[0], (OutScalar)tail[1] }; } };
    }
    template <typename T_out, int N_head, int N_tail, typename T_head, typename T_tail>
    inline T_out vector_concat(const T_head& head, const T_tail& tail) {
        return internal::VectorConcat<T_out, N_head, N_tail, T_head, T_tail>::impl(head, tail);
    }
}
