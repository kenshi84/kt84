#pragma once
#include <iterator>
#include <array>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/optional.hpp>
#undef max
#undef min

namespace kt84 {
namespace container_util {
    namespace internal {
        template<class Container_to, class Container_from>
        struct Cast {
            inline static Container_to doCast(const Container_from& c) {
                return Container_to(c.begin(), c.end());
            }
        };
        template<class T, int N, class Container_from>
        struct Cast<std::array<T,N>, Container_from> {
            inline static std::array<T,N> doCast(const Container_from& c) {
                std::array<T,N> result;
                auto i1 = c.begin();
                auto i2 = result.begin();
                for (; i2 != result.end(); ++i1, ++i2)
                    *i2 = *i1;
                return result;
            }
        };

    }
    template<class Container_to, class Container_from>
    inline Container_to cast(const Container_from& c) {
        return internal::Cast<Container_to, Container_from>::doCast(c);
    }
    template<class Container1, class Container2>
    inline auto set_intersection(Container1&& sortedRange1, Container2&& sortedRange2) {
        std::vector<std::remove_reference_t<decltype(*std::begin(sortedRange1))>> result;
        boost::set_intersection(sortedRange1, sortedRange2, std::back_inserter(result));
        return result;
    }
    template<class Container1, class Container2>
    inline auto set_union(Container1&& sortedRange1, Container2&& sortedRange2) {
        std::vector<std::remove_reference_t<decltype(*std::begin(sortedRange1))>> result;
        boost::set_union(sortedRange1, sortedRange2, std::back_inserter(result));
        return result;
    }
    template<class Container1, class Container2>
    inline auto set_difference(Container1&& sortedRange1, Container2&& sortedRange2) {
        std::vector<std::remove_reference_t<decltype(*std::begin(sortedRange1))>> result;
        boost::set_difference(sortedRange1, sortedRange2, std::back_inserter(result));
        return result;
    }
    template<class Container1, class Container2>
    inline auto set_symmetric_difference(Container1&& sortedRange1, Container2&& sortedRange2) {
        std::vector<std::remove_reference_t<decltype(*std::begin(sortedRange1))>> result;
        boost::set_symmetric_difference(sortedRange1, sortedRange2, std::back_inserter(result));
        return result;
    }
    template <class Container1, class Container2>
    inline int count_intersection(Container1&& c1, Container2&& c2) {
        int result = 0;
        for (auto& v1 : c1)
            if (boost::range::find(c2, v1)!=c2.end())
                ++result;
        return result;
    }
    template <class Container>
    inline bool insert_if_nonexistent(Container& c, const decltype(*c.begin())& val) {
        if (boost::range::find(c, val) == c.end()) {
            c.insert(val);
            return true;
        }
        return false;
    }
    template<class Map>
    inline boost::optional<typename Map::mapped_type> at_optional(Map& map, const typename Map::key_type& key) {
        auto p = map.find(key);
        if (p == map.end())
            return boost::none;
        else
            return p->second;
    }
    template<class Container>
    inline int mod_index(const Container& c, int index) {
        while (index < 0) index += c.size();
        return index % c.size();
    }
    template<class Container>
    inline auto at_mod(Container& c, int index) -> decltype(c[index]) {
        return c[mod_index(c,index)];
    }
    template<class Container>
    inline auto at_mod(const Container& c, int index) -> decltype(c[index]) {
        return c[mod_index(c,index)];
    }
    /*
    Boost equivalent `boost::range::remove_erase_if' exists in <boost/range/algorithm_ext/erase.hpp>
    template<class Container, class Predicate>
    inline void remove_if(Container& c, Predicate pred) {
        c.erase(boost::range::remove_if(c, pred), c.end());
    }
    */
    template<class Container, class T>
    inline void bring_front(Container& c, const T& val) {
        boost::range::rotate(c, boost::range::find(c, val));
    }
    template<class Container, class UnaryPredicate> 
    inline void bring_front_if(Container& c, UnaryPredicate p) {
        boost::range::rotate(c, boost::range::find_if(c, p));
    }
    template<class Container> 
    inline auto erase_at(Container& c, int index) -> decltype(c.erase(c.begin())) {
        auto pos = c.begin();
        std::advance(pos, index);
        return c.erase(pos);
    }
    template<class Container>
    inline void remove_duplicate(Container& c) {
        boost::range::sort(c);
        auto new_last = boost::range::unique(c).end();
        c.erase(new_last, c.end());
    }
    template <class Container>
    inline auto max(const Container& c, int& index) -> decltype(*c.begin()) {
        auto i = c.begin();
        auto result = *i;
        index = 0;
        for (++i; i != c.end(); ++i, ++index)
            if (result < *i) result = *i;
        return result;
    }
    template <class Container>
    inline auto min(const Container& c, int& index) -> decltype(*c.begin()) {
        auto i = c.begin();
        auto result = *i;
        index = 0;
        for (++i; i != c.end(); ++i, ++index)
            if (result > *i) result = *i;
        return result;
    }
    template <class Container>
    inline auto max(const Container& c) -> decltype(*c.begin()) {
        int index;
        return max(c, index);
    }
    template <class Container>
    inline auto min(const Container& c) -> decltype(*c.begin()) {
        int index;
        return min(c, index);
    }
    template <class Container>
    inline auto sum(const Container& c) -> decltype(*c.begin()) {
        auto i = c.begin();
        auto result = *i;
        for (++i; i != c.end(); ++i)
            result += *i;
        return result;
    }
    template <class Container, class Range>
    inline void append(Container& c, const Range& range) {
        boost::range::copy(range, std::back_inserter(c));
    }
    template <class Container, class Range>
    inline void push_back(Container& c, const Range& range) { append(c,range); }
    template <class Container, class Range>
    inline void push_front(Container& c, const Range& range) {
        boost::range::copy(range, std::front_inserter(c));
    }
    template <class Container, class Range>
    inline void insert_front(Container& c, const Range& range) {
        boost::range::copy(range, std::inserter(c,c.begin()));
    }
    template <class Container, class Range>
    inline void insert_back(Container& c, const Range& range) {
        boost::range::copy(range, std::inserter(c,c.end()));
    }
    namespace internal {
        template <class Container>
        struct All {
            const Container& container;
            All(const Container& container): container(container) {}
            template <typename Rhs> bool operator==(const Rhs& rhs) const { for (auto& e : container) if (!(e == rhs)) return false; return true; }
            template <typename Rhs> bool operator!=(const Rhs& rhs) const { for (auto& e : container) if (!(e != rhs)) return false; return true; }
            template <typename Rhs> bool operator< (const Rhs& rhs) const { for (auto& e : container) if (!(e <  rhs)) return false; return true; }
            template <typename Rhs> bool operator> (const Rhs& rhs) const { for (auto& e : container) if (!(e >  rhs)) return false; return true; }
            template <typename Rhs> bool operator<=(const Rhs& rhs) const { for (auto& e : container) if (!(e <= rhs)) return false; return true; }
            template <typename Rhs> bool operator>=(const Rhs& rhs) const { for (auto& e : container) if (!(e >= rhs)) return false; return true; }
        };
        template <class Container>
        struct Any {
            const Container& container;
            Any(const Container& container): container(container) {}
            template <typename Rhs> bool operator==(const Rhs& rhs) const { for (auto& e : container) if (e == rhs) return true; return false; }
            template <typename Rhs> bool operator!=(const Rhs& rhs) const { for (auto& e : container) if (e != rhs) return true; return false; }
            template <typename Rhs> bool operator< (const Rhs& rhs) const { for (auto& e : container) if (e <  rhs) return true; return false; }
            template <typename Rhs> bool operator> (const Rhs& rhs) const { for (auto& e : container) if (e >  rhs) return true; return false; }
            template <typename Rhs> bool operator<=(const Rhs& rhs) const { for (auto& e : container) if (e <= rhs) return true; return false; }
            template <typename Rhs> bool operator>=(const Rhs& rhs) const { for (auto& e : container) if (e >= rhs) return true; return false; }
        };
    }
    template <class Container> inline internal::All<Container> all(const Container& c) { return internal::All<Container>(c); }
    template <class Container> inline internal::Any<Container> any(const Container& c) { return internal::Any<Container>(c); }
}
}
