#pragma once
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <kt84/detail/tetgen.hh>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

namespace kt84 {
    namespace tetgen_util {
        struct Format_node {
            std::vector<double> pointlist;
            std::vector<double> pointattributelist;
            std::vector<int   > pointmarkerlist;
            void write(std::ofstream& ofs) const {
                ofs << "# <# of points> <dimension (3)> <# of attributes> <boundary markers (0 or 1)>" << std::endl;
                const size_t numberofpoints = pointlist.size()/3;
                const size_t numberofpointattributes = pointattributelist.size()/numberofpoints;
                ofs << numberofpoints << "  3  " << numberofpointattributes << "  " << (pointmarkerlist.empty() ? 0 : 1) << std::endl;
                for (size_t i = 0; i < numberofpoints; ++i) {
                    if (i==0) ofs << "# <point #> <x> <y> <z> [attributes] [boundary marker]" << std::endl;
                    ofs << i << " ";
                    for (size_t j = 0; j < 3; ++j)
                        ofs << " " << pointlist[3*i + j];
                    for (size_t j = 0; j < numberofpointattributes; ++j)
                        ofs << " " << pointattributelist[numberofpointattributes*i + j];
                    if (!pointmarkerlist.empty())
                        ofs << " " << pointmarkerlist[i];
                    ofs << std::endl;
                }
            }
            void write(const std::string& filename_base) const {
                std::ofstream ofs(filename_base + ".node");
                write(ofs);
            }
        };
        struct Format_poly {
            Format_node node;
            struct Facet {
                std::vector<std::vector<int>> polygonlist;
                std::vector<double> holelist;
            };
            std::vector<Facet> facetlist;       //std::vector<tetgenio::facet> facetlist;
            std::vector<int> facetmarkerlist;
            std::vector<double> holelist;
            std::vector<double> regionlist;
            void write(const std::string& filename_base) const {
                std::ofstream ofs(filename_base + ".poly");
                ofs << "## Part 1 - node list" << std::endl;
                node.write(ofs);
                ofs << std::endl;
                ofs << "## Part 2 - facet list" << std::endl;
                ofs << "# <# of facets> <boundary markers (0 or 1)>" << std::endl;
                const size_t numberoffacets = facetlist.size();
                ofs << numberoffacets << "  " << (facetmarkerlist.empty() ? 0 : 1) << std::endl;
                for (size_t i = 0; i < numberoffacets; ++i) {
                    const Facet& facet = facetlist[i];
                    if (i==0) ofs << "# <# of polygons> [# of holes] [boundary marker]" << std::endl;
                    const size_t numberofpolygons = facet.polygonlist.size();
                    const size_t numberofholes = facet.holelist.size()/3;
                    ofs << numberofpolygons << "  " << numberofholes << std::endl;
                    if (!facetmarkerlist.empty())
                        ofs << "  " << facetmarkerlist[i];
                    for (size_t j = 0; j < numberofpolygons; ++j) {
                        if (i==0 && j==0) ofs << "# <# of corners> <corner 1> <corner 2> ... <corner #>" << std::endl;
                        ofs << facet.polygonlist[j].size() << " ";
                        for (size_t k = 0; k < facet.polygonlist[j].size(); ++k)
                            ofs << " " << facet.polygonlist[j][k];
                        ofs << std::endl;
                    }
                    for (size_t j = 0; j < numberofholes; ++j) {
                        if (i==0 && j==0) ofs << "# <hole #> <x> <y> <z>" << std::endl;
                        ofs << j << " ";
                        for (size_t k = 0; k < 3; ++k)
                            ofs << " " << facet.holelist[3*j + k];
                        ofs << std::endl;
                    }
                }
                ofs << std::endl;
                ofs << "## Part 3 - hole list" << std::endl;
                ofs << "# <# of holes>" << std::endl;
                const size_t numberofholes = holelist.size()/3;
                ofs << numberofholes << std::endl;
                for (size_t i = 0; i < numberofholes; ++i) {
                    if (i==0) ofs << "# <hole #> <x> <y> <z>" << std::endl;
                    ofs << i << " ";
                    for (size_t j = 0; j < 3; ++j)
                        ofs << " " << holelist[3*i + j];
                    ofs << std::endl;
                }
                ofs << std::endl;
                ofs << "## Part 4 - region list" << std::endl;
                ofs << "# <# of region>" << std::endl;
                const size_t numberofregions = regionlist.size()/5;
                ofs << numberofregions << std::endl;
                for (size_t i = 0; i < numberofregions; ++i) {
                    if (i==0) ofs << "# <region #> <x> <y> <z> <region number> <region attribute>" << std::endl;
                    ofs << i << " ";
                    for (size_t j = 0; j < 5; ++j)
                        ofs << " " << regionlist[5*i + j];
                    ofs << std::endl;
                }
            }
        };
        struct Format_smesh {
            Format_node node;
            std::vector<std::vector<int>> facetlist;
            std::vector<int> facetmarkerlist;
            std::vector<double> holelist;
            std::vector<double> regionlist;
            
            // conversion to .poly
            operator Format_poly() const {
                Format_poly poly;
                poly.node = node;
                poly.facetlist.reserve(facetlist.size());
                for (auto& f : facetlist)
                    poly.facetlist.push_back({{f}, {}});
                poly.facetmarkerlist = facetmarkerlist;
                poly.holelist = holelist;
                poly.regionlist = regionlist;
                return poly;
            }
        };
        struct Format_off {
            std::vector<double> vertices;
            std::vector<std::vector<int>> faces;
            
            // conversion to .poly
            operator Format_poly() const {
                Format_poly poly;
                poly.node.pointlist = vertices;
                poly.facetlist.reserve(faces.size());
                for (auto& f : faces)
                    poly.facetlist.push_back({{f}, {}});
                return poly;
            }
            void write(const std::string& filename_base) const {
                std::ofstream ofs(filename_base + ".off");
                assert(vertices.size()%3 == 0);
                const size_t numVertices = vertices.size()/3;
                const size_t numFaces = faces.size();
                ofs << "OFF " << numVertices << " " << numFaces << " 0" << std::endl;
                for (size_t i = 0; i < numVertices; ++i) {
                    for (int j = 0; j < 3; ++j)
                        ofs << (j==0? "" : " ") << vertices[3*i + j];
                    ofs << std::endl;
                }
                for (size_t i = 0; i < numFaces; ++i) {
                    const size_t nv = faces[i].size();
                    ofs << nv;
                    for (size_t j = 0; j < nv; ++j)
                        ofs << " "  << faces[i][j];
                    ofs << std::endl;
                }
            }
            bool read(const std::string& filename_base) {
                std::ifstream ifs(filename_base + ".off");
                std::string str;
                bool magic_read = false;
                size_t numVertices;
                size_t numFaces;
                size_t numEdges;
                std::vector<double> read_vertices;
                std::vector<std::vector<int>> read_faces;
                while (std::getline(ifs, str)) {
                    boost::tokenizer<boost::char_separator<char>> tokens_raw(str, boost::char_separator<char>(" "));
                    std::vector<std::string> tokens(tokens_raw.begin(), tokens_raw.end());
                    if (tokens.empty() || tokens[0][0] == '#')
                        continue;
                    if (!magic_read) {
                        if (tokens.size() != 4 || tokens[0] != "OFF")
                            return false;
                        try {
                            numVertices = boost::lexical_cast<size_t>(tokens[1]);
                            numFaces = boost::lexical_cast<size_t>(tokens[2]);
                            numEdges = boost::lexical_cast<size_t>(tokens[3]);
                        } catch (const boost::bad_lexical_cast& e) {
                            return false;
                        }
                        if (numEdges != 0)
                            return false;
                        magic_read = true;
                        continue;
                    }
                    if (read_vertices.size()/3 < numVertices) {
                        if (tokens.size() != 3)
                            return false;
                        double x, y, z;
                        try {
                            read_vertices.push_back(boost::lexical_cast<double>(tokens[0]));
                            read_vertices.push_back(boost::lexical_cast<double>(tokens[1]));
                            read_vertices.push_back(boost::lexical_cast<double>(tokens[2]));
                        } catch (const boost::bad_lexical_cast& e) {
                            return false;
                        }
                        continue;
                    }
                    if (read_faces.size() < numFaces) {
                        size_t deg;
                        try {
                            deg = boost::lexical_cast<size_t>(tokens[0]);
                        } catch (const boost::bad_lexical_cast& e) {
                            return false;
                        }
                        if (tokens.size() != 1+deg)
                            return false;
                        read_faces.push_back({});
                        for (size_t i = 0; i < deg; ++i) {
                            try {
                                read_faces.back().push_back(boost::lexical_cast<size_t>(tokens[1+i]));
                            } catch (const boost::bad_lexical_cast& e) {
                                return false;
                            }
                        }
                    }
                }
                if (read_vertices.size()/3 < numVertices || read_faces.size() < numFaces)
                    return false;
                vertices = read_vertices;
                faces = read_faces;
                return true;
            }
        };
        struct Format_ele {
            int numberofcorners = 4;
            std::vector<int> tetrahedronlist;
            std::vector<double> tetrahedronattributelist;
            // TODO: tetrahedronvolumelist
            void write(const std::string& filename_base) const {
                std::ofstream ofs(filename_base + ".ele");
                ofs << "# <# of tetrahedra> <nodes per tet. (4 or 10)> <region attribute (0 or 1)>" << std::endl;
                const size_t numberoftetrahedra = tetrahedronlist.size()/numberofcorners;
                ofs << numberoftetrahedra << " " << numberofcorners << " " << (tetrahedronattributelist.empty() ? 0 : 1) << std::endl;
                for (size_t i = 0; i < numberoftetrahedra; ++i) {
                    if (i==0) ofs << "# <tetrahedron #> <node> <node> ... <node> [attribute]" << std::endl;
                    ofs << i << " ";
                    for (int j = 0; j < numberofcorners; ++j)
                        ofs << " " << tetrahedronlist[numberofcorners*i + j];
                    if (!tetrahedronattributelist.empty())
                        ofs << " " << tetrahedronattributelist[i];
                    ofs << std::endl;
                }
            }
        };
        struct Format_face {
            std::vector<int> trifacelist;
            std::vector<int> trifacemarkerlist;
            void write(const std::string& filename_base) const {
                std::ofstream ofs(filename_base + ".face");
                ofs << "# <# of faces> <boundary marker (0 or 1)>" << std::endl;
                const size_t numberoftrifaces = trifacelist.size()/3;
                ofs << numberoftrifaces << "  " << (trifacemarkerlist.empty() ? 0 : 1) << std::endl;
                for (size_t i = 0; i < numberoftrifaces; ++i) {
                    if (i==0) ofs << "# <face #> <node> <node> <node> ... [boundary marker] ..." << std::endl;
                    ofs << i << " ";
                    for (int j = 0; j < 3; ++j)
                        ofs << " " << trifacelist[3*i + j];
                    if (!trifacemarkerlist.empty())
                        ofs << " " << trifacemarkerlist[i];
                    ofs << std::endl;
                }
            }
        };
        struct Format_edge {
            std::vector<int> edgelist;
            std::vector<int> edgemarkerlist;
            void write(const std::string& filename_base) const {
                std::ofstream ofs(filename_base + ".edge");
                ofs << "# <# of edges> <boundary marker (0 or 1)>" << std::endl;
                const size_t numberofedges = edgelist.size()/2;
                ofs << numberofedges << "  " << (edgemarkerlist.empty() ? 0 : 1) << std::endl;
                for (size_t i = 0; i < numberofedges; ++i) {
                    if (i==0) ofs << "# <edge #> <endpoint> <endpoint> ... [boundary marker] ..." << std::endl;
                    ofs << i << " ";
                    for (int j = 0; j < 2; ++j)
                        ofs << " " << edgelist[2*i + j];
                    if (!edgemarkerlist.empty())
                        ofs << " " << edgemarkerlist[i];
                    ofs << std::endl;
                }
            }
        };
        struct Format_neigh {
            std::vector<int> neighborlist;
            void write(const std::string& filename_base) const {
                std::ofstream ofs(filename_base + ".neigh");
                ofs << "# <# of tetrahedra>  4" << std::endl;
                const size_t numberoftetrahedra = neighborlist.size()/4;
                ofs << numberoftetrahedra << "  4" << std::endl;
                for (size_t i = 0; i < numberoftetrahedra; ++i) {
                    if (i==0) ofs << "# <tetrahedra #> <neighbor> <neighbor> <neighbor> <neighbor>" << std::endl;
                    ofs << i << " ";
                    for (int j = 0; j < 4; ++j)
                        ofs << " " << neighborlist[4*i + j];
                    ofs << std::endl;
                }
            }
        };
        struct Out {
            Format_node node;
            Format_ele ele;
            Format_face face;
            Format_edge edge;
            Format_neigh neigh;
            void write(const std::string& filename_base) const {
                node.write(filename_base);
                ele.write(filename_base);
                face.write(filename_base);
                edge.write(filename_base);
                if (!neigh.neighborlist.empty()) neigh.write(filename_base);
            }
        };
        struct Switches {
            // Tetrahedralizes a piecewise linear complex (PLC).
            bool p = false;

            // Preserves the input surface mesh (does not modify it).
            bool Y = false;

            // Refines mesh (to improve mesh quality).
            //  - the first constraint is the maximum allowable radius-edge ratio
            //  - the second constraint is the minimum allowable dihedral angle (in degree)
            bool q = false;
            double q_ratio = 2.0;
            double q_angle = 0.0;

            // Assigns attributes to tetrahedra in different regions.
            // The -A switch assigns an additional attribute (an integer number) to each tetrahedron that identifies to what facet-bounded region each tetrahedron belongs.
            // In the output mesh, all tetrahedra in the same region will get a corresponding non-zero attribute.
            //  - A region attribute is an integer which can be either positive or negative. It must not be a zero, which is used for the exterior of the PLC.
            //  - User-defined attributes are assigned to regions by the .poly or .smesh file (in the region section). If a region is not explicitly marked by
            //      the .poly file or .smesh file, tetrahedra in that region are automatically assigned a non-zero attribute.
            //  - By default, the region attributes are numbered from 1, 2, 3, .... If there are user-assigned region attributes (by the .poly or .smesh file),
            //      the region attributes are shifted by a number i, i.e., i+1, i+2, i+3, ..., where i is either 0 or the maximum integer of the user-assigned region attributes.
            //  - The -A switch has an effect only if the -p switch is used and the -r switch is not.
            bool A = false;

            // Applies a maximum tetrahedron volume constraint.
            //  - If a number follows the -a, no tetrahedra is generated whose volume is larger than that number.
            //  - (unsupported) If no number is specified and the -r switch is used, a .vol file is expected, which contains a separate volume constraint for each tetrahedron.
            bool a_fixed = false;
            double a_fixed_value;
            // bool a_varying = false;

            // Specifies the level of mesh optimization.
            // - The mesh optimization level is an integer ranged from 0 to 10, where 0 means no mesh optimization is executed.
            // - There are three local operations available in TetGen for optimizing the mesh, which are: edge/face flips, vertex smoothing, and vertex insertion/deletion.
            int O_level = 2;
            bool O_edge_face_flips = true;
            bool O_vertex_smoothing = true;
            bool O_vertex_insertion_deletion = true;

            // Specifies maximum number of added points.
            // The default is to allow an unlimited number of Steiner points.
            int S = -1;

            // Sets a tolerance for coplanar test (default 10^-8).
            // In principle, the vertices which are used to define a facet of a PLC should be exactly coplanar. But this is very hard to achieve in practice due to
            // the inconsistent nature of the floating-point format used in computers.
            // TetGen accepts facets whose vertices are not exactly but "approximately coplanar". Four points a, b, c and d are assumed to be coplanar as long as
            // the ratio v/l^3 is smaller than the given tolerance, where v and l are the volume and the average edge length of the tetrahedron abcd, respectively.
            // To choose a proper tolerance according to the input point set will usually reduce the number of adding points and improve the mesh quality.
            double T = 1.0e-8;

            // Suppresses use of exact arithmetic.
            bool X = false;

            // No merge of coplanar facets or very close vertices.
            bool M = false;

            // Generates weighted Delaunay (regular) triangulation.
            // The points in .node file must have at least one attribute, and the first attribute of each point is its weight.
            bool w = false;

            // Retains the convex hull of the PLC.
            bool c = false;

            // Outputs all faces to .face file.
            bool f = false;

            // Outputs all edges to .edge file.
            bool e = false;

            // Outputs tetrahedra neighbors to .neigh file.
            bool n = false;

            // No jettison of unused vertices from output .node file.
            bool J = false;

            // Suppresses output of boundary information.
            bool B = false;

            // Suppresses output of .node file.
            bool N = false;

            // Suppresses output of .ele file.
            bool E = false;

            // Suppresses output of .face and .edge file.
            bool F = false;

            // Checks the consistency of the final mesh.
            bool C = false;

            // Quiet: No terminal output except errors.
            bool Q = false;

            // Verbose: Detailed information, more terminal output.
            bool V = false;

            // Other unsupported but possibly relevant flags:
            //  -r Reconstructs a previously generated mesh.
            //  -R Mesh coarsening (to reduce the mesh elements).
            //  -m Applies a mesh sizing function.
            //  -i Inserts a list of additional points.
            //  -d Detects self-intersections of facets of the PLC.
            //  -v Outputs Voronoi diagram to files.

            std::string to_string() const {
                std::ostringstream res;
                res << std::setprecision(30) << std::fixed;
                if (p) res << "p";
                if (Y) res << "Y";
                if (q) res << "q" << q_ratio << "/" << q_angle;
                if (A) res << "A";
                if (a_fixed) res << "a" << a_fixed_value;
                res << "O" << O_level << "/" << (O_edge_face_flips ? 1 : 0) + (O_vertex_smoothing ? 2 : 0) + (O_vertex_insertion_deletion ? 4 : 0);
                if (S != -1) res << "S" << S;
                res << "T" << T;
                if (X) res << "X";
                if (M) res << "M";
                if (w) res << "w";
                if (c) res << "c";
                if (f) res << "f";
                if (e) res << "e";
                if (n) res << "n";
                if (J) res << "J";
                if (B) res << "B";
                if (N) res << "N";
                if (E) res << "E";
                if (F) res << "F";
                if (C) res << "C";
                if (Q) res << "Q";
                if (V) res << "V";
                return res.str();
            }
        };
        inline Out tetrahedralize(const Format_poly& in_poly, const std::string& switches) {
            using namespace detail;
            tetgenio in;
            //----+
            // IN |
            //----+
            // points
            in.numberofpoints = (int)in_poly.node.pointlist.size() / 3;
            in.pointlist      = const_cast<double*>(&in_poly.node.pointlist[0]);
            // point attributes
            in.numberofpointattributes = (int)in_poly.node.pointattributelist.size() / in.numberofpoints;
            if (!in_poly.node.pointattributelist.empty())
                in.pointattributelist = const_cast<double*>(&in_poly.node.pointattributelist[0]);
            // point boundary markers
            if (!in_poly.node.pointmarkerlist.empty())
                in.pointmarkerlist = const_cast<int*>(&in_poly.node.pointmarkerlist[0]);
            // faces
            in.numberoffacets = (int)in_poly.facetlist.size();
            std::vector<tetgenio::facet> facetlist(in.numberoffacets);
            std::vector<std::vector<tetgenio::polygon>> facetpolygonlist(in.numberoffacets);
            if (in.numberoffacets > 0)
                in.facetlist = &facetlist[0];
            for (int i = 0; i < in.numberoffacets; ++i) {
                facetpolygonlist[i].reserve(in_poly.facetlist[i].polygonlist.size());
                for (auto& p : in_poly.facetlist[i].polygonlist)
                    facetpolygonlist[i].push_back({const_cast<int*>(&p[0]), (int)p.size()});
                in.facetlist[i].numberofpolygons = (int)facetpolygonlist[i].size();
                in.facetlist[i].polygonlist = &facetpolygonlist[i][0];
                in.facetlist[i].numberofholes = (int)in_poly.facetlist[i].holelist.size() / 3;
                in.facetlist[i].holelist = in.facetlist[i].numberofholes ? const_cast<double*>(&in_poly.facetlist[i].holelist[0]) : nullptr;
            }
            // holes
            in.numberofholes = (int)in_poly.holelist.size() / 3;
            if (!in_poly.holelist.empty())
                in.holelist = const_cast<double*>(&in_poly.holelist[0]);
            // regions
            in.numberofregions = (int)in_poly.regionlist.size() / 5;
            if (!in_poly.regionlist.empty())
                in.regionlist = const_cast<double*>(&in_poly.regionlist[0]);
            
            tetgenio out;
            detail::tetrahedralize(const_cast<char*>(switches.c_str()), &in, &out);
            
            //-----+
            // OUT |
            //-----+
            Format_node out_node;
            // pointlist
            out_node.pointlist.resize(3 * out.numberofpoints);
            for (size_t i = 0; i < out_node.pointlist.size(); ++i)
                out_node.pointlist[i] = out.pointlist[i];
            // pointattributelist
            out_node.pointattributelist.resize(out.numberofpointattributes * out.numberofpoints);
            for (size_t i = 0; i < out_node.pointattributelist.size(); ++i)
                out_node.pointattributelist[i] = out.pointattributelist[i];
            // pointmarkerlist
            if (out.pointmarkerlist != nullptr) {
                out_node.pointmarkerlist.resize(out.numberofpoints);
                for (size_t i = 0; i < out_node.pointmarkerlist.size(); ++i)
                    out_node.pointmarkerlist[i] = out.pointmarkerlist[i];
            }
            
            Format_ele out_ele;
            // tetrahedronlist
            out_ele.numberofcorners = out.numberofcorners;
            out_ele.tetrahedronlist.resize(out.numberofcorners * out.numberoftetrahedra);
            for (size_t i = 0; i < out_ele.tetrahedronlist.size(); ++i)
                out_ele.tetrahedronlist[i] = out.tetrahedronlist[i];
            if (out.tetrahedronattributelist != nullptr) {
                out_ele.tetrahedronattributelist.resize(out.numberoftetrahedronattributes * out.numberoftetrahedra);
                for (size_t i = 0; i < out_ele.tetrahedronattributelist.size(); ++i)
                    out_ele.tetrahedronattributelist[i] = out.tetrahedronattributelist[i];
            }

            Format_face out_face;
            // trifacelist
            out_face.trifacelist.resize(3 * out.numberoftrifaces);
            for (size_t i = 0; i < out_face.trifacelist.size(); ++i)
                out_face.trifacelist[i] = out.trifacelist[i];
            // trifacemarkerlist
            if (out.trifacemarkerlist != nullptr) {
                out_face.trifacemarkerlist.resize(out.numberoftrifaces);
                for (int i = 0; i < out.numberoftrifaces; ++i)
                    out_face.trifacemarkerlist[i] = out.trifacemarkerlist[i];
            }
            
            Format_edge out_edge;
            // edgelist
            out_edge.edgelist.resize(2 * out.numberofedges);
            for (size_t i = 0; i < out_edge.edgelist.size(); ++i)
                out_edge.edgelist[i] = out.edgelist[i];
            // edgemarkerlist
            if (out.edgemarkerlist != nullptr) {
                out_edge.edgemarkerlist.resize(out.numberofedges);
                for (int i = 0; i < out.numberofedges; ++i)
                    out_edge.edgemarkerlist[i] = out.edgemarkerlist[i];
            }
            
            Format_neigh out_neigh;
            // neighborlist
            if (out.neighborlist != nullptr) {
                out_neigh.neighborlist.resize(4 * out.numberoftetrahedra);
                for (size_t i = 0; i < out_neigh.neighborlist.size(); ++i)
                    out_neigh.neighborlist[i] = out.neighborlist[i];
            }
            
            in.initialize();
            return { out_node, out_ele, out_face, out_edge, out_neigh };
        }
        inline Out tetrahedralize(const Format_poly& in_poly, const Switches& switches) {
            return tetrahedralize(in_poly, switches.to_string());
        }
        inline Out tetrahedralize(const Format_node& in_node, const Switches& switches) {
            Format_poly in_poly;
            in_poly.node = in_node;
            return tetrahedralize(in_poly, switches);
        }
        inline Out tetrahedralize(const Format_off& in_off, const Switches& switches) {
            Format_poly in_poly = in_off;
            return tetrahedralize(in_poly, switches);
        }
    }
}
