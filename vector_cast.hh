#pragma once
#include <type_traits>

namespace kt84 {
    namespace internal {
        // to achieve partial specialization on function template
        template <int N, typename T_to, typename T_from>
        struct VectorCast {
            static T_to cast(const T_from& v) {
                T_to result;
                for (int i = 0; i < N; ++i)
                    result[i] = v[i];
                return result;
            }
            template <typename CastFunc>
            static T_to cast(const T_from& v, CastFunc f) {
                T_to result;
                for (int i = 0; i < N; ++i)
                    result[i] = f(v[i]);
                return result;
            }
        };
        template <typename T_to, typename T_from>
        struct VectorCast<2, T_to, T_from> {
            static T_to cast(const T_from& v) {
                static T_to dummy;
                using ToScalar = std::remove_reference_t<decltype(dummy[0])>;
                return { (ToScalar)v[0], (ToScalar)v[1] };
            }
            template <typename CastFunc>
            static T_to cast(const T_from& v, CastFunc f) { return { f(v[0]), f(v[1]) }; }
        };
        template <typename T_to, typename T_from>
        struct VectorCast<3, T_to, T_from> {
            static T_to cast(const T_from& v) {
                static T_to dummy;
                using ToScalar = std::remove_reference_t<decltype(dummy[0])>;
                return { (ToScalar)v[0], (ToScalar)v[1], (ToScalar)v[2] };
            }
            template <typename CastFunc>
            static T_to cast(const T_from& v, CastFunc f) { return { f(v[0]), f(v[1]), f(v[2]) }; }
        };
        template <typename T_to, typename T_from>
        struct VectorCast<4, T_to, T_from> {
            static T_to cast(const T_from& v) {
                static T_to dummy;
                using ToScalar = std::remove_reference_t<decltype(dummy[0])>;
                return { (ToScalar)v[0], (ToScalar)v[1], (ToScalar)v[2], (ToScalar)v[3] };
            }
            template <typename CastFunc>
            static T_to cast(const T_from& v, CastFunc f) { return { f(v[0]), f(v[1]), f(v[2]), f(v[3]) }; }
        };
    }
    template <int N, typename T_to, typename T_from>
    inline T_to vector_cast(const T_from& v) {
        return internal::VectorCast<N, T_to, T_from>::cast(v);
    }
    template <int N, typename T_to, typename T_from, typename CastFunc>
    inline T_to vector_cast(const T_from& v, CastFunc f) {
        return internal::VectorCast<N, T_to, T_from>::cast(v, f);
    }
    template <typename T_to, typename T_from>
    inline T_to vector_cast(unsigned int N, const T_from& v) {
        T_to result;
        result.resize(N);
        for (unsigned int i = 0; i < N; ++i)
            result[i] = v[i];
        return result;
    }
    template <typename T_to, typename T_from, typename CastFunc>
    inline T_to vector_cast(unsigned int N, const T_from& v, CastFunc f) {
        T_to result;
        result.resize(N);
        for (unsigned int i = 0; i < N; ++i)
            result[i] = f(v[i]);
        return result;
    }
}
