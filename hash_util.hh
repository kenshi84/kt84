#pragma once
#include <utility>
#include <array>

namespace kt84 { namespace hash_util {
// https://stackoverflow.com/questions/2590677/how-do-i-combine-hash-values-in-c0x
template <class T>
inline void hash_combine(std::size_t& seed, const T& v) {
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}
}}

namespace std {

template<class T1, class T2>
struct hash<pair<T1,T2>> {
    size_t operator()(const pair<T1,T2>& x) const {
        size_t h = 0;
        kt84::hash_util::hash_combine(h, x.first);
        kt84::hash_util::hash_combine(h, x.second);
        return h;
    }
};

template <class T, size_t N>
struct hash<array<T,N>> {
    size_t operator()(const array<T,N>& c) const {
        size_t h = 0;
        for (auto& e : c)
            kt84::hash_util::hash_combine(h, e);
        return h;
    }
};

template <template<class,class> class Container, class T1, class T2>
struct hash<Container<T1,T2>> {
    size_t operator()(const Container<T1,T2>& c) const {
        size_t h = 0;
        for (auto& e : c)
            kt84::hash_util::hash_combine(h, e);
        return h;
    }
};

template <template<class,class,class> class Container, class T1, class T2, class T3>
struct hash<Container<T1,T2,T3>> {
    size_t operator()(const Container<T1,T2,T3>& c) const {
        size_t h = 0;
        for (auto& e : c)
            kt84::hash_util::hash_combine(h, e);
        return h;
    }
};

template <template<class,class,class,class> class Container, class T1, class T2, class T3, class T4>
struct hash<Container<T1,T2,T3,T4>> {
    size_t operator()(const Container<T1,T2,T3,T4>& c) const {
        size_t h = 0;
        for (auto& e : c)
            kt84::hash_util::hash_combine(h, e);
        return h;
    }
};

template <template<class,class,class,class,class> class Container, class T1, class T2, class T3, class T4, class T5>
struct hash<Container<T1,T2,T3,T4,T5>> {
    size_t operator()(const Container<T1,T2,T3,T4,T5>& c) const {
        size_t h = 0;
        for (auto& e : c)
            kt84::hash_util::hash_combine(h, e);
        return h;
    }
};

}
