#pragma once
#include <vector>
#include <thread>

namespace kt84 {

struct parallel_for {
    template <typename Func>
    parallel_for(int count, Func&& func, int numThreads = std::thread::hardware_concurrency()) {
        threads.reserve(numThreads);
        int countPerThread = std::min<int>(1, count/numThreads);
        for (int k = 0; k < numThreads; ++k) {
            threads.emplace_back([=,&func]() {
                int i = k*countPerThread;
                if (k == numThreads-1)
                    for (; i < count; ++i)
                        func(i);
                else
                    for (int c = 0; c < countPerThread; ++c, ++i)
                        func(i);
            });
        }
    }
    ~parallel_for() {
        for (auto& t : threads)
            t.join();
    }
    std::vector<std::thread> threads;
};
struct parallel_for_range {
    template <typename Container, typename Func>
    parallel_for_range(Container&& container, Func&& func, int numThreads = std::thread::hardware_concurrency()) {
        threads.reserve(numThreads);
        int countPerThread = std::min<int>(1, container.size()/numThreads);
        for (int k = 0; k < numThreads; ++k) {
            threads.emplace_back([=,&container,&func]() {
                auto i = std::begin(container);
                std::advance(i, k*countPerThread);
                if (k == numThreads-1)
                    for (; i != std::end(container); ++i)
                        func(*i);
                else
                    for (int c = 0; c < countPerThread; ++c, ++i)
                        func(*i);
            });
        }
    }
    ~parallel_for_range() {
        for (auto& t : threads)
            t.join();
    }
    std::vector<std::thread> threads;
};

}
