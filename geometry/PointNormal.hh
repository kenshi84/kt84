#pragma once

#include <Eigen/Core>

namespace kt84 {

typedef Eigen::Matrix<double, 6, 1> PointNormal;
inline PointNormal pn_make(const Eigen::Vector3d& point, const Eigen::Vector3d& normal) { PointNormal pn; pn << point, normal; return pn; }
inline PointNormal pn_make(const Eigen::Vector3f& point, const Eigen::Vector3f& normal) { PointNormal pn; pn << point.cast<double>(), normal.cast<double>(); return pn; }
inline double pn_norm(const PointNormal& pn) { return pn.head(3).norm(); }
inline void   pn_normalize(PointNormal& pn) { pn.tail(3).normalize(); }
inline PointNormal pn_normalized(const PointNormal& pn) { auto temp = pn; temp.tail(3).normalize(); return temp; }

typedef Eigen::Matrix<double, 4, 1> PointNormal2d;
inline PointNormal2d pn2d_make(const Eigen::Vector2d& point, const Eigen::Vector2d& normal) { PointNormal2d pn; pn << point, normal; return pn; }
inline PointNormal2d pn2d_make(const Eigen::Vector2f& point, const Eigen::Vector2f& normal) { PointNormal2d pn; pn << point.cast<double>(), normal.cast<double>(); return pn; }
inline double pn2d_norm(const PointNormal2d& pn) { return pn.head(2).norm(); }
inline void   pn2d_normalize(PointNormal2d& pn) { pn.tail(2).normalize(); }
inline PointNormal2d pn2d_normalized(const PointNormal2d& pn) { auto temp = pn; temp.tail(2).normalize(); return temp; }

}
