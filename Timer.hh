#pragma once
#include <chrono>
#include <iostream>
#include <sstream>
#include <memory>

namespace kt84 {

struct Timer {
    Timer(bool autostart = true) {
        if (autostart)
            start();
    }
    void start() {
        if (active)
            return;
        active = true;
        before = std::chrono::high_resolution_clock::now();
    }
    void stop() {
        if (!active)
            return;
        active = false;
        update();
    }
    void reset() {
        active = false;
        duration = std::chrono::high_resolution_clock::duration::zero();
    }
    void restart() {
        reset();
        start();
    }
    auto nanoseconds () { update(); return std::chrono::duration_cast<std::chrono::nanoseconds >(duration).count(); }
    auto microseconds() { update(); return std::chrono::duration_cast<std::chrono::microseconds>(duration).count(); }
    auto milliseconds() { update(); return std::chrono::duration_cast<std::chrono::milliseconds>(duration).count(); }
    auto seconds     () { update(); return std::chrono::duration_cast<std::chrono::seconds     >(duration).count(); }
    auto minutes     () { update(); return std::chrono::duration_cast<std::chrono::minutes     >(duration).count(); }
    auto hours       () { update(); return std::chrono::duration_cast<std::chrono::hours       >(duration).count(); }
    std::string nanoseconds_str () { std::stringstream ss; ss << nanoseconds () << "ns" ; return ss.str(); }
    std::string microseconds_str() { std::stringstream ss; ss << microseconds() << "us" ; return ss.str(); }
    std::string milliseconds_str() { std::stringstream ss; ss << milliseconds() << "ms" ; return ss.str(); }
    std::string seconds_str     () { std::stringstream ss; ss << seconds     () << "s"  ; return ss.str(); }
    std::string minutes_str     () { std::stringstream ss; ss << minutes     () << "min"; return ss.str(); }
    std::string hours_str       () { std::stringstream ss; ss << hours       () << "h"  ; return ss.str(); }
protected:
    void update() {
        auto now = std::chrono::high_resolution_clock::now();
        duration += now - before;
        before = now;
    }
    bool active = false;
    std::chrono::high_resolution_clock::time_point before;
    std::chrono::high_resolution_clock::duration duration = std::chrono::high_resolution_clock::duration::zero();
};

namespace timer {
template <typename Func> inline void printElapsed_ns(const std::string& message, Func&& func, std::ostream& out = std::cout) { out << message << "...\n"; Timer t; func(); out << message << "..." << t.nanoseconds_str() << std::endl; }
template <typename Func> inline void printElapsed_us(const std::string& message, Func&& func, std::ostream& out = std::cout) { out << message << "...\n"; Timer t; func(); out << message << "..." << t.microseconds_str() << std::endl; }
template <typename Func> inline void printElapsed_ms(const std::string& message, Func&& func, std::ostream& out = std::cout) { out << message << "...\n"; Timer t; func(); out << message << "..." << t.milliseconds_str() << std::endl; }
template <typename Func> inline void printElapsed_s(const std::string& message, Func&& func, std::ostream& out = std::cout) { out << message << "...\n"; Timer t; func(); out << message << "..." << t.seconds_str() << std::endl; }
template <typename Func> inline void printElapsed_m(const std::string& message, Func&& func, std::ostream& out = std::cout) { out << message << "...\n"; Timer t; func(); out << message << "..." << t.minutes_str() << std::endl; }
template <typename Func> inline void printElapsed_h(const std::string& message, Func&& func, std::ostream& out = std::cout) { out << message << "...\n"; Timer t; func(); out << message << "..." << t.hours_str() << std::endl; }
}

struct ScopedTimerPrinter {
    ScopedTimerPrinter(const std::string& message, std::ostream& out = std::cout, const std::string& unit = "ms") : message(message), out(out), unit(unit) {
        out << message << " ... " << std::endl;
    }
    ~ScopedTimerPrinter() {
        out << message << " ... " << (
            unit == "ns" ? timer.nanoseconds_str() :
            unit == "us" ? timer.microseconds_str() :
            unit == "s" ? timer.seconds_str() :
            unit == "m" ? timer.minutes_str() :
            unit == "h" ? timer.hours_str() : timer.milliseconds_str()
            ) << std::endl;
    }
protected:
    Timer timer;
    std::string message;
    std::ostream& out;
    std::string unit;
};

inline std::shared_ptr<ScopedTimerPrinter> make_ScopedTimerPrinter(const std::string& message, std::ostream& out = std::cout, const std::string& unit = "ms") {
    return std::make_shared<ScopedTimerPrinter>(message, out, unit);
}

}
